import glob
import os
import pandas as pd
from pathlib import Path
import re
import datetime
import time

Parent_Path = r'C:\Users\Sb103\Desktop\Neuer Ordner'


Rename_checker, renamed_list_of_images = Image_renamer()  
    
def Dimension_Checker(image_to_be_checked):
    os.chdir(Folder_Path)
    Accessible_Images = []
    Inaccessible_Images = []

    image_Path_meta = os.path.join(Folder_Path, image)
    image_meta = Image.open(image_Path_meta)
    image_meta.close()
    image_meta_string = str(image_meta)
    image_meta_string_split = image_meta_string.split()

    if ("size=1936x1216" in image_meta_string_split):  # x=1936 (width) and y=1216 (height)
        Accessible_Images.append(image)

    else:
        Inaccessible_Images.append(image)

    return Accessible_Images, Inaccessible_Images

if Rename_checker == True:
    for image in renamed_list_of_images:
        Dimension_Checker(image)

else:
    for image in list_of_images_sorted:
        Dimension_Checker(image)

Image_count = 0
for item in Accessible_Images:
    if Rename_checker == True:
        for image in renamed_list_of_images:
            Image_count += 1

        if Image_count in range(First_Image, Last_Image):
            Pre_Amp_Image_counter.append(Image_count)
            Pre_Amp_Images.append(image)

    else:
        for image in list_of_images_sorted:
            Image_count += 1

        if Image_count in range(First_Image, Last_Image):
            Pre_Amp_Image_counter.append(Image_count)
            Pre_Amp_Images.append(image)


# creating a 0 to 381 y koordinates for the Cropped_Zone and reversing it to 381 to 0
yKoordinates = list(reversed(range(y, y + 381)))  
# removing first elements by the len(inacessible_images) from y Koordinates for the graph. This variable doesn't get outputted anymore, but it can be useful
yKoordinates = yKoordinates[len(Inaccessible_Images) :]



############################################################################
# Script for analyzing Strobo log file
for item in glob.iglob(os.path.join(Parent_Path, '*.txt')):
    name = Path(item).stem
    df =pd.read_csv(item, sep=';')


    Times = df.iloc[:, 0].tolist() # Times of when images are taken by strobo are stored in the first column
    Frequences_list = df.iloc[:, 3].tolist() # Frequences are stored in the fourth column

    # Extracting indeces of all relevant images based on relevant frequencies (25, 20, 65, 15)
    Frequences_list = [round(f) for f in Frequences_list]
    _25Hz_images_i = [i for i,f in enumerate(Frequences_list) if f == 25]
    _20Hz_images_i = [i for i,f in enumerate(Frequences_list) if (f == 20) & (i >= 310) & (i <= 330)]
    _65Hz_images_i = [i for i,f in enumerate(Frequences_list) if f > 64]
    _15Hz_images_i = [i for i,f in enumerate(Frequences_list) if (f == 15) & (i >= 440) & (i <= 460)]
    _50Hz_images_i = [i for i,f in enumerate(Frequences_list) if f == 50]
    
    _25Hz_image = _25Hz_images_i[-1] # Last 25Hz image before fluid reaches PreAmp-Lyo chamber
    _25Hz_image_time = df.iloc[_25Hz_image, 0]

    _20Hz_image = _20Hz_images_i[-1] # Last 20Hz image before pumping in channel 12
    _20Hz_image_time = df.iloc[_20Hz_image, 0]

    _65Hz_image = _65Hz_images_i[0] # First image over 64Hz after pumping in channel 12
    _65Hz_image_time = df.iloc[_65Hz_image, 0]

    _15Hz_image = _15Hz_images_i[-1] # Last 15Hz image after metering finger
    _15Hz_image_time = df.iloc[_15Hz_image, 0]

    _50Hz_image = _50Hz_images_i[0]
    _50Hz_image_time = df.iloc[_50Hz_image, 0]

print('Last 25Hz image is; ' + _25Hz_image_time)
print('Last 20Hz image is: ' + _20Hz_image_time)
print('First 65Hz image is: ' + _65Hz_image_time)
print('Last 15Hz image is: ' + _15Hz_image_time)
