import cv2 as cv
import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from scipy.optimize import curve_fit, fmin
import copy


def rotate_matrix(
    x, y, angle, rotation_center_x=0, rotation_center_y=0, units="DEGREES"
):
    """Rotates a point in the xy-plane counterclockwise through an angle about the origin
    https://en.wikipedia.org/wiki/Rotation_matrix
    Parameters
    ----------
    x : float
        x coordinate
    y : float
        y coordinate
    angle : float
        _description_
    rotation_center_x : float, optional
        x-axis shift from origin (0, 0), by default 0
    rotation_center_y : float, optional
        y-axis shift from origin (0, 0), by default 0
    units : str, optional
        The rotation angle in degrees or radians, by default "DEGREES"
    Returns
    -------
    xy : Tuple
        Tuple of rotated x and y
    """

    # Shift to origin (0,0)
    x = x - rotation_center_x
    y = y - rotation_center_y

    # Convert degrees to radians
    if units == "DEGREES":
        angle = np.deg2rad(angle)

    # Rotation matrix multiplication to get rotated x & y
    xr = (x * np.cos(angle)) - (y * np.sin(angle)) + rotation_center_x
    yr = (x * np.sin(angle)) + (y * np.cos(angle)) + rotation_center_y

    return xr, yr


def rotate_image_general(image, angle, image_center="auto"):
    """Rotates pixel color values within a fixed canvas (keeps axes fixed).
    negative angle: leftwise rotation (against the clock)
    positive angle: rightwise rotation (with the clock)
    Parameters
    ----------
    image : array
        image read with imread
    angle : float
        angle in degree
    image_center : str, 2dtuple, 2dlist, optional
        center position of image in pixels. If set to "auto" calculates image center. Else a coordiante float tuple can be passed, by default "auto"
    Returns
    -------
    result: array
        rotated array
    warp_mat: 2d list
        rotationmatrix used for transform
    """
    if image_center == "auto":
        image_center = tuple(np.array(image.shape[1::-1]) / 2)

    rot_mat = cv.getRotationMatrix2D(image_center, -angle, 1.0)
    result = cv.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv.INTER_LINEAR)

    # fig, ax = plt.subplots(1, 1)
    # ax.imshow(result, cmap="gray", vmin=0, vmax=255)

    return result, rot_mat


def scale_image(image, percent, maxwh):
    max_width = maxwh[1]
    max_height = maxwh[0]
    max_percent_width = max_width / image.shape[1] * 100
    max_percent_height = max_height / image.shape[0] * 100
    max_percent = 0
    if max_percent_width < max_percent_height:
        max_percent = max_percent_width
    else:
        max_percent = max_percent_height
    if percent > max_percent:
        percent = max_percent
    width = int(image.shape[1] * percent / 100)
    height = int(image.shape[0] * percent / 100)
    result = cv.resize(image, (width, height), interpolation=cv.INTER_AREA)
    return (result,)


def get_cropped_strobo_template(image, cropzone=[460, 1050, 1050, 1530]):
    """Extracts a rectangular part of the template image defined by cropzone parameters.

    Parameters
    ----------
    image : np.array
        image read by cv imread
    cropzone : list[int]
        crop idx sequence: bot top left right.

    Returns
    -------
    image_cropped : np.array
        pixel values of all pixels inside the cropzone
    global_edge_coords : list[int]
        pixel coordinates of the croprectangle corners
    image_whitecropped : np.array
        original image set to white except for the cropped window. (useful for overlay testing)
    """

    # cropzone = [460, 1050, 1050, 1530]  # [460, 1050, 1050, 1830]

    image_cropped = image[cropzone[0] : cropzone[1], cropzone[2] : cropzone[3]]
    height_cropped, width_cropped = image_cropped.shape

    image_whitecropped = copy.deepcopy(image)
    image_whitecropped[0 : cropzone[0], :] = np.array(255).astype(np.uint8)
    image_whitecropped[cropzone[1] :, :] = np.array(255).astype(np.uint8)
    image_whitecropped[:, cropzone[3] :] = np.array(255).astype(np.uint8)
    image_whitecropped[:, 0 : cropzone[2]] = np.array(255).astype(np.uint8)

    rect = patches.Rectangle(
        [cropzone[1], cropzone[0]],
        width_cropped,
        height_cropped,
        0,
        fill=False,
        edgecolor="r",
    )  # positive angle for counterclockwise rotation
    # top right, top left, bot left, botright
    corners = rect.get_patch_transform().transform([(0, 0), (1, 0), (1, 1), (0, 1)])
    global_edge_coords = corners

    fig, ax = plt.subplots(1, 2)
    ax[0].imshow(image_whitecropped, cmap="gray", vmin=0, vmax=255)
    ax[1].imshow(image_cropped, cmap="gray", vmin=0, vmax=255)
    ax[0].add_patch(rect)

    return image_cropped, global_edge_coords, image_whitecropped


def shift_imageby_warp_matrix(image, warp_matrix) -> np.array:
    """Shifts an image based on a warp matrix defined by the cv2 (cv.warpAffine) package.

    Parameters
    ----------
    image : np.array
        image read by cv imread
    warp_matrix : np.array
        warpmatrix defined by cv2 package

    Returns
    -------
    transformed_img : np.array
        warps an image but keeps the canvas (axes) fixed.
    """
    transformed_img = cv.warpAffine(
        image, warp_matrix, (image.shape[1], image.shape[0])
    )
    return transformed_img


def run_affine_matching_from_pathfiles(reference_path, image_path):
    """runs affine matching using pathfiles as input.

    Parameters
    ----------
    reference_path : str
        path to reference picture where template is generated from
    image_path : str
        image in which the match should be found

    Returns
    -------
    _aligned_image : np.array
        transformed image, aligned to reference image
    warp_mat : np.array
        warp matrix for cv affine transformation
    """
    # Read the images to be aligned
    im_ref = cv.imread(reference_path)
    im_trans = cv.imread(image_path)

    im_ref_grey = cv.cvtColor(im_ref, cv.COLOR_BGR2GRAY)
    im_shift_grey = cv.cvtColor(im_trans, cv.COLOR_BGR2GRAY)

    (
        im_ref_grey,
        img_ref_global_coords,
        ref_image_whitecropped,
    ) = get_cropped_strobo_template(im_ref_grey)

    _aligned_image, warp_mat = template_matched_affine_transformation(
        im_ref_grey,
        im_shift_grey,
        img_ref_global_coords,
        ref_image_whitecropped,
        steps_fine=0.1,
    )

    fig, ax = plt.subplots(1, 1)
    ax.imshow(ref_image_whitecropped, cmap="gray")
    ax.imshow(_aligned_image, cmap="gray", alpha=0.3)
    fig.tight_layout()

    return _aligned_image, warp_mat


def template_matched_affine_transformation(
    im_ref_grey: np.array,
    im_shift_grey: np.array,
    img_ref_global_coords: np.array,
    ref_image_whitecropped: np.array,
    init_angle: float = -20,
    end_angle: float = 20,
    delta_rough: int = 2,
    steps_fine: float = 0.1,
) -> np.array:
    """Adaptive rotation invariant matched template algorithm
    This algorithm takes a reference greyscale image and an shifted image which should be aligned.
    The reference image has significant morphological featue overlap with the shifted image and has smaller dimensions than the shifted image.
    The algorithm finds the template in the image including rotational differences.
    The shifted image is then shifted to match the reference template.
    The init angle and end angle define the range of rotation angles that the shifted image is estimated to lie within.
    Should this be unknown set to -180, 180 or 0, 360. This will come at the cost of increased runtime.
    Should a finer estimation be needed the parameter steps_fine can be decreased to infer greater rotational accuracy.

    #TODO
    #upsampling before running algo - could increase
    #check Translation part for errors
    #make curvefitting algorithm work
    #add scaling option (function already exists, integrate in algorithm)

    Parameters
    ----------
    im_ref_grey : np.array
        reference template
    im_shift_grey : np.array
        image that is rotated towards reference image
    img_ref_global_coords : np.array
        global pixel position of the reference image in the original picture it was taken from
    init_angle : float, optional
        start of range of rotation angles that the shifted image is estimated to lie within, by default -10
    end_angle : float, optional
        end of range of rotation angles that the shifted image is estimated to lie within, by default 10
    delta_rough : int, optional
        stepsize within rotation range for rough angle estimation, by default 2
    steps_fine : float, optional
        final angle estimation accuracy, by default 0.01

    Returns
    -------
    transformed_img : np.array
        image shifted and aligned to match reference template

    Examples
    --------
    >>> REF_PATH = Path("Referenz_Bild_Warm.jpg")
    >>> IMAGE_PATH = Path("alignment_test_data/270_001109.jpg")
    >>> aligned_image, warp_mat = run_affine_matching_from_pathfiles(REF_PATH.as_posix(), IMAGE_PATH.as_posix())
    """

    def rotation_invariant_template_matching(
        im_ref_gray, im_trans_gray, angles, method=cv.TM_CCOEFF
    ):
        result_pnts = []
        height, width = im_ref_gray.shape
        for angle in angles:
            rotated_template, _ = rotate_image_general(im_ref_gray, angle)
            matched_points = cv.matchTemplate(im_trans_gray, rotated_template, method)
            min_val, max_val, min_loc, max_loc = cv.minMaxLoc(matched_points)

            # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
            # points are in x,y format while image is in y,x format! However, hight increases towards -y -> thus +hight instead of minus to get to lower pixels
            if method in [cv.TM_SQDIFF, cv.TM_SQDIFF_NORMED]:
                top_left = min_loc
            else:
                top_left = max_loc
            top_right = [top_left[0] + width, top_left[1]]
            bottom_right = [top_left[0] + width, top_left[1] + height]
            bottom_left = [top_left[0], top_left[1] + height]
            corners_manual = [top_left, top_right, bottom_right, bottom_left]
            corners_center = [top_left[0] + (width / 2), top_left[1] + (height / 2)]

            corners_rot_man = np.array(
                [
                    rotate_matrix(
                        c[0],
                        c[1],
                        angle,
                        corners_center[0],
                        corners_center[1],
                        units="DEGREES",
                    )
                    for c in corners_manual
                ]
            )

            # rect = patches.Rectangle(
            #     bottom_left, width, -height, angle
            # )  # positive angle for counterclockwise rotation
            # corners_rot_rect = rect.get_patch_transform().transform(
            #     [(0, 0), (1, 0), (1, 1), (0, 1)]
            # )

            # corners_rot_man_check = [
            #     rotate_matrix(
            #         c[0],
            #         c[1],
            #         angle,
            #         bottom_left[0],
            #         bottom_left[1],
            #         units="DEGREES",
            #     )
            #     for c in corners_manual
            # ]

            corners = corners_rot_man  # corners_rot_rect  # corners_rot_man

            # fig, ax = plt.subplots(3, 1)
            # ax[0].imshow(matched_points, cmap="gray")
            # ax[1].imshow(rotated_template, cmap="gray")
            # ax[2].imshow(im_trans_gray, cmap="gray")
            # ax[0].add_patch(copy.copy(rect))
            # ax[2].add_patch(rect)
            # ax[2].plot(
            #     corners_center[0], corners_center[1], "r*", linewidth=5, markersize=10
            # )

            result_pnts.append(
                [min_val, max_val, top_left, bottom_right, angle, corners]
            )

        fig, ax = plt.subplots(1, 1)
        ax.plot([r[1] for r in result_pnts])

        # find angle with max correlation
        max_cor_idx = np.argmax([el[1] for el in result_pnts])
        max_cor_angle = result_pnts[max_cor_idx][4]
        corners_bbox_max_angle = result_pnts[max_cor_idx][5]
        print(f"The maximum correlation was found at angle: {max_cor_angle}")
        return max_cor_angle, corners_bbox_max_angle

    # values
    height_cropped, width_cropped = im_ref_grey.shape

    methods = [
        # "cv.TM_CCOEFF",
        # "cv.TM_CCOEFF_NORMED",
        # "cv.TM_CCORR",
        "cv.TM_CCORR_NORMED",
        # "cv.TM_SQDIFF",
        # "cv.TM_SQDIFF_NORMED",
    ]
    for me in methods:
        me_eval = eval(me)
        # rough angle estimation
        steps_rough = round((end_angle - init_angle) / delta_rough + 1)
        angles = np.linspace(init_angle, end_angle, steps_rough)
        max_cor_angle_rough, _ = rotation_invariant_template_matching(
            im_ref_grey, im_shift_grey, angles, method=me_eval
        )

        # finetuned template matching
        max_cor_angle_fine = np.linspace(
            round(max_cor_angle_rough - (delta_rough / 2)),
            round(max_cor_angle_rough + (delta_rough / 2)),
            round((delta_rough / steps_fine) + 1),
        )

        _, corners = rotation_invariant_template_matching(
            im_ref_grey, im_shift_grey, max_cor_angle_fine, method=me_eval
        )

        # fig, ax = plt.subplots(1, 2)
        # ax[0].imshow(im_shift_grey, cmap="gray")
        # ax[0].plot([c[0] for c in corners], [c[1] for c in corners], "-g")
        # ax[1].imshow(ref_image_whitecropped, cmap="gray")
        # fig.tight_layout()

        # warping image back to reference image bevore cropping
        warp_mat_fine = cv.getAffineTransform(
            np.float32(corners[[1, 0, 3]]),
            np.float32(img_ref_global_coords[[1, 0, 3]]),
        )
        transformed_img = cv.warpAffine(
            im_shift_grey,
            warp_mat_fine,
            (im_shift_grey.shape[1], im_shift_grey.shape[0]),
        )

        fig, ax = plt.subplots(1, 1)
        ax.imshow(ref_image_whitecropped, cmap="gray")
        ax.imshow(transformed_img, cmap="gray", alpha=0.5)
        fig.tight_layout()
        fig.show()
        fig.suptitle(f"Method: {me}", fontsize=16)

        plt.savefig(f"Method_{me}_.png", dpi=150)
        # cv.imwrite("alignment_test.png", transformed_img)
        print("Done")

    return transformed_img, warp_mat_fine


REF_PATH = Path(r"""Referenz_Bild_Warm.jpg""")
# IMAGE_PATH = Path(r"""Referenz_Bild_Warm.jpg""")
# REF_PATH = Path(r"""alignment_test_data/Referenz_Bild_cropped2.jpg""")
# IMAGE_PATH = Path(
#    r"""C:\Users\MN100\Documents\Arbeitsumgebung\GIT\SARA\Neuer Ordner\Warme_Strobo_Test_imagesnotaligned\270\Bild 0.jpg"""
# )

# IMAGE_PATH = Path(
#    r"""C:\Users\MN100\Documents\Arbeitsumgebung\GIT\SARA\270_001109.jpg"""
# )

IMAGE_PATH = Path(
    r"""C:\Users\MN100\Documents\Arbeitsumgebung\GIT\SARA\algo_casefiles\respi_run_case1.jpg"""
)

# IMAGE_PATH = Path(
#    r"""C:\Users\MN100\Documents\Arbeitsumgebung\GIT\SARA\algo_casefiles\ref_run_case2.jpg"""
# )

import time

tic = time.perf_counter()
# _aligned_image = ORB_algorithm(REF_PATH.as_posix(), IMAGE_PATH.as_posix())
# _aligned_image = align_image(REF_PATH.as_posix(), IMAGE_PATH.as_posix())

_aligned_image, warp_mat = run_affine_matching_from_pathfiles(
    REF_PATH.as_posix(), IMAGE_PATH.as_posix()
)


toc = time.perf_counter()
print(f"Elapsed time is: {toc - tic:0.4f} seconds")

# Testing warp
# im_shift = cv.imread(IMAGE_PATH.as_posix(), cv.IMREAD_GRAYSCALE)
# _aligned_image = shift_imageby_warp_matrix(im_shift, warp_mat)


im_ref = cv.imread(REF_PATH.as_posix())
fig, ax = plt.subplots(1, 1)
ax.imshow(im_ref, cmap="gray")
ax.imshow(_aligned_image, cmap="gray", alpha=0.3)
fig.tight_layout()
cv.imwrite("alignment_test.png", _aligned_image)

print("Done")
