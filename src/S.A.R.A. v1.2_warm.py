import cv2 as cv
import os
from os import getpid
import glob
import re
import shutil
import copy as copy

from PIL import Image
import numpy as np
from scipy import ndimage

import matplotlib.pyplot as plt
import matplotlib.patches as patches

import time
import psutil
from tqdm import tqdm

import itertools
import xlsxwriter

#from math import sin
#from math import cos
#from math import radians

#############AUTHOR:############semibrami@gmail.com#####################################
# User Inputs
Main_Path = input("Enter folder PATH (MAIN): ")
Parent_Path_list = os.listdir(Main_Path)
First_Image, Last_Image = map(int, input("Enter First_Image and Last_Image: ").split())

# reference_path = input('Enter Reference Image PATH: ')
reference_path = os.path.abspath(r"Referenz_Bild_Warm.jpg") # abspath will look at all the files in PC, pinpoint and pick only the Path of reference image (runs very vast)
Rename = "yes"
Rotate = "yes"

# Starting total timer, starting CPU and RAM measurement, starting to count Analysis-Runs
psutil.cpu_percent()
Run_counter = 0
absolute_zero_seconds = time.time()

# Choose a Strobo folder to analyze. All 4 Degree Folders will be analyzed
def Folder_analyzer():
    global Folder_Path
    global Raw_Path
    global target_Folder

    os.chdir(Main_Path) # Changing working directory MAIN folder

    """Main Parent Function running all other big functions

    Parameters
    ----------
    Main_Path (global): string
        path of MAIN folder (the path which was input when SARA was started)
        MAIN folder contains all strobo run folders to be analyzed

    Returns
    -------
    Folder_Path (global): string
        path of every strobo run folder
        this path is identical to Raw_Path, but changes if folder has been renamed

    Raw_Path (global): string
        path of every angle folder
        this path is identical to Folder_Path, but doesn't change if folder has been renamed

    Renamed_List_of_folders : list of strings
        list of all new paths of renamed Angle folders within Strobo Run folders

    target_Folder (global): string
        path of "S.A.R.A Analysis" folder, where all results and images will be saved
    """
    
    # Creates a .txt file to check if analysis is over and allows re-analysis
    def Create_Run_checker():
        global Checker_path

        """Function creating a .txt file to check if run was analyzed successfully or not, also allows for analysis repetition if this .txt file is deleted

        Parameters
        ----------
        Checker_path (global): string
            path of Checker
            checker is a text file which will be created within analyzing strobo folder and named "ANALYZING" 
            it will be renamed to "Analysis_successful_READ_ME.txt" after SARA Analysis is conducted successfully
            this text file must be deleted in order to repeat a SARA analysis

        Parent_Path (global): string
            path of every strobo run folder

        Returns
        -------
        Checker_path (global): string
            path including modified name of the checker
        """

        Checker_name = "ANALYZING.txt"
        Checker_path = os.path.join(Parent_Path, Checker_name)

        return Checker_path

    # Creating Target Folder (S.A.R.A Analysis) in parent directory, skipping if it already exists
    def Create_analysis_folder():
        global Rotate
        global Rename

        """Function creating the S.A.R.A Analysis folder, also known as "target_Folder" where all results and images from SARA will be saved
           all files within this folder must be deleted in case of a needed repeated analysis
           it also changes Rotate & Rename variables to "no" if this folder already exists

        Parameters
        ----------
        Rotate (global): string
            this string == "yes" (if images need to be rotated) or "no" (if not)

        Rename (global): string
            this string == "yes" (if images need to be renamed) or "no" (if not)

        Returns
        -------
        target_Folder (global): string
            path of S.A.R.A Analysis folder
        """

        target_Folder_name = "S.A.R.A Analysis"
        mode = 0o666
        target_Folder = os.path.join(Parent_Path, target_Folder_name)

        if os.path.isdir(target_Folder):
            Rotate = "no"
            Rename = "no"

        else:
            os.mkdir(target_Folder, mode)

        return target_Folder

    # Creating Backup folder with original files and original folder names in parent directory, skipping if it already exists
    def Create_backup_folder():
        
        """Function creating 1 new backup folder for each angle folder within all strobo run folders
           backup folders contain all the original unmodified images

        Parameters
        ----------
        Raw_Path (global): string
            path of every angle folder
            this path is identical to Folder_Path, but doesn't change if folder has been renamed

        Parent_Path (global): string
            path of every strobo run folder

        Folder_Path (global): string
            path of every strobo run folder
            this path is identical to Raw_Path, but changes if folder has been renamed    

        Returns
        -------
        target_Folder (global): string
            path of S.A.R.A Analysis folder
        """
                
        Backup_Folder = os.path.basename(Raw_Path)
        os.chdir(os.path.dirname(Raw_Path))

        Backup_path = os.path.join(Parent_Path, "Backup " + str(Backup_Folder))
        if os.path.isdir(Backup_path):  # passing if backup was already created
            pass
        else:
            shutil.copytree(Folder_Path, "Backup " + Backup_Folder)

        return Backup_path

    def Back_side_analyzer():

        """Function copying the 10th-from-last image in 1° and 180° folders into the target_Folder
           this image contains the filled End-Chambers 
           originally designed to also run additional analysis, like: Channel 21 analysis and Siphon analysis

        Parameters
        ----------
        list_of_images : list of strings
            list of all image paths within each angle folder

        target_Folder (global): string
            path of S.A.R.A Analysis folder

        Folder_Path (global): string
            path of every strobo run folder
            this path is identical to Raw_Path, but changes if folder has been renamed    

        Returns
        -------
        Endkammer_image : string
            path of image containing filled End-Chambers
        """

        # Finding image of filled end chambers (10th-from-last image)
        list_of_images = os.listdir(Folder_Path)
        Endkammer_image = list_of_images[-10]
        Endkammer_image_path = os.path.join(Folder_Path, Endkammer_image)

        # Getting the base name of found Endchamber image and adding EndK to its name
        temporary = Endkammer_image.split(".")
        Endkammer_image = temporary[0]
        new_Endkammer_image = (
            Endkammer_image + "_" + str(os.path.basename(Parent_Path)) + ".png"
        )
        New_Endkammer_image_path = str(os.path.join(Folder_Path, new_Endkammer_image))
        os.rename(Endkammer_image_path, New_Endkammer_image_path)

        # Copying the found & renamed endchamber image to target_Folder
        shutil.copy(New_Endkammer_image_path, target_Folder)

        return Endkammer_image

    # Creating a progressbar tracking progress after each folder analysis
    for Runs in tqdm(Parent_Path_list, total=len(Parent_Path_list), desc="Progress Bar"):
        """For Loop running all other functions for each strobo run folder (also named here as "Runs") within MAIN folder
           a progressbar will be created within python console to track the progress after each folder analysis 

        Parameters
        ----------
        Parent_Path (global): string
            path of every strobo run folder

        Main_Path (global): string
            path of MAIN folder (the path which was input when SARA was started)
            MAIN folder contains all strobo run folders to be analyzed

        Folder_Path (global): string
            path of every strobo run folder
            this path is identical to Raw_Path, but changes if folder has been renamed

        Raw_Path (global): string
            path of every angle folder
            this path is identical to Folder_Path, but doesn't change if folder has been renamed    
        """

        Parent_Path = os.path.join(Main_Path, Runs)
        List_of_folders = os.listdir(Parent_Path)
        Renamed_List_of_folders = []
        Checker_path = Create_Run_checker()
        target_Folder = Create_analysis_folder()

        os.chdir(Parent_Path) # changing working directory to each Strobo Run folder

        # For Loop checking if Strobo Run Folders are renamed (meaning, the ° was deleted from the name). Checks if folders are named "1", "90", "180" or "270"
        for Folder_name in List_of_folders:
            if Folder_name.endswith("°"):
                if os.path.basename(Checker_path) == "ANALYZING.txt":
                    Raw_Path = os.path.join(Parent_Path, Folder_name)

                    if "Backup" not in os.path.basename(Raw_Path):
                        Folder_Path = Raw_Path[:-1]
                        os.rename(Raw_Path, Folder_Path)
                        Renamed_List_of_folders.append(Folder_Path)

                        if os.path.basename(Folder_Path) == "1":
                            Create_backup_folder()
                            Back_side_analyzer()

                        if os.path.basename(Folder_Path) == "90":
                            Create_backup_folder()
                            Front_side_analyzer()

                        if os.path.basename(Folder_Path) == "180":
                            Create_backup_folder()
                            Back_side_analyzer()

                        if os.path.basename(Folder_Path) == "270":
                            Create_backup_folder()
                            Front_side_analyzer()
            else:
                if os.path.basename(Checker_path) == "ANALYZING.txt":
                    Raw_Path = os.path.join(Parent_Path, Folder_name)
                    Folder_Path = Raw_Path

                    if os.path.basename(Folder_Path) == "1":
                        Back_side_analyzer()

                    if os.path.basename(Folder_Path) == "90":
                        Front_side_analyzer()

                    if os.path.basename(Folder_Path) == "180":
                        Back_side_analyzer()

                    if os.path.basename(Folder_Path) == "270":
                        Front_side_analyzer()

        # Renaming Run_checker if run is successful
        New_Checker_path = os.path.join(Parent_Path, "Analysis_successful_READ_ME.txt")
        os.rename(Checker_path, New_Checker_path)

    return Renamed_List_of_folders


def Front_side_analyzer():
    global First_Image
    global Last_Image
    global x
    global y
    global Degrees
    global average
    global ROI_Epsilon

    """Big Function containing all the functions necessary for Pre_Amp analysis 
        originally designed to also run additional analysis, like: Channel 8 analysis

        Parameters
        ----------
        Folder_Path (global): string
            path of every strobo run folder
            this path is identical to Raw_Path, but changes if folder has been renamed 

        First_Image (global): integer
            input by user before starting SARA
            used to find the image with that count within angle folder (only folders 270 and 90)
            first image to be analyzed for Pre_Amp analysis

        Last_Image (global): integer
            input by user before starting SARA
            used to find the image with that count within angle folder (only folders 270 and 90)  
            last image to be analyzed for Pre_Amp analysis

        Returns
        -------
        x (global): integer
            defined x of the left corner of the Cropped_Zone for the left chamber

        y (global): integer
            defined y of the left corner of the Cropped_Zone for the left chamber

        Degrees (global): integer (can be float)
            angle which will be used to rotate the analyzed images, so that meniscus in the left chamber is as horizontal as possible

        average (global): integer
            the best plausible meniscus position (Y Coordinate)
            this value will then be appended to a list and then plotted to show the position of meniscii during the run

        ROI_Epsilon (global): integer
            arbitrary region of interest, which defines a range of pixels
            starts as 15
            in case the next found meniscus is further than the defined range from the previous meniscus, then that found meniscus is discarded in favor of other values
            EXCEPTION: if average < 305 and meniscus still outside range, then ROI_Epsilon == 70
            this value restricts found meniscii using logical flow of the fluid, thus emitting too high or too low poisitions of meniscus
        """
    
    ROI_Epsilon = 15  # Epsilon restricting cropped zone ROI by using the previous meniscus. 15 is an arbitrary value because usually volume increases by 5 pixels per image
    average = 381  # Used for Meniscus_Optimizer function

    # Start time measurement
    zero_seconds = time.time()

    x = 1208
    y = 705

    positions_of_meniscus = (
        []
    )  # list containing all y-coordinate from all detected meniscii
    Pre_Amp_Images = []  # list containing the names of only PreAmp Images
    Pre_Amp_Image_counter = []  # list counting PreAmp images from the very first image
    Inaccessible_Images = (
        []
    )  # list of images that look black and don't have equal dimensions as other images; these images cannot be further analyzed

    # Replacing directory in the right place
    os.chdir(Folder_Path)
    # Re-adjusting first image and last image index for python
    First_Image = First_Image
    Last_Image = Last_Image + 1

    # Creating a list of all image strings from Dir_Path
    list_of_images = os.listdir(Folder_Path)
    list_of_images_sorted = sorted(
        list_of_images, key=lambda f: float(re.findall("(\d+)", f)[0])
    )

    # Returns warp_matrix & registered image ### this needs to be fixed ###

    # ====================================

    def rotate_matrix(
        x, y, angle, rotation_center_x=0, rotation_center_y=0, units="DEGREES"
    ):
        """Rotates a point in the xy-plane counterclockwise through an angle about the origin
        https://en.wikipedia.org/wiki/Rotation_matrix

        Parameters
        ----------
        x : float
            x coordinate
        y : float
            y coordinate
        angle : float
            _description_
        rotation_center_x : float, optional
            x-axis shift from origin (0, 0), by default 0
        rotation_center_y : float, optional
            y-axis shift from origin (0, 0), by default 0
        units : str, optional
            The rotation angle in degrees or radians, by default "DEGREES"

        Returns
        -------
        xy : Tuple
            Tuple of rotated x and y
        """

        # Shift to origin (0,0)
        x = x - rotation_center_x
        y = y - rotation_center_y

        # Convert degrees to radians
        if units == "DEGREES":
            angle = np.deg2rad(angle)

        # Rotation matrix multiplication to get rotated x & y
        xr = (x * np.cos(angle)) - (y * np.sin(angle)) + rotation_center_x
        yr = (x * np.sin(angle)) + (y * np.cos(angle)) + rotation_center_y

        return xr, yr

    def rotate_image_general(image, angle, image_center="auto"):
        """Rotates pixel color values within a fixed canvas (keeps axes fixed).
        negative angle: leftwise rotation (against the clock)
        positive angle: rightwise rotation (with the clock)

        Parameters
        ----------
        image : array
            image read with imread
        angle : float
            angle in degree
        image_center : str, 2dtuple, 2dlist, optional
            center position of image in pixels. If set to "auto" calculates image center. Else a coordiante float tuple can be passed, by default "auto"

        Returns
        -------
        result: array
            rotated array
        warp_mat: 2d list
            rotationmatrix used for transform
        """
        if image_center == "auto":
            image_center = tuple(np.array(image.shape[1::-1]) / 2)

        rot_mat = cv.getRotationMatrix2D(image_center, -angle, 1.0)
        result = cv.warpAffine(
            image, rot_mat, image.shape[1::-1], flags=cv.INTER_LINEAR
        )

        # fig, ax = plt.subplots(1, 1)
        # ax.imshow(result, cmap="gray", vmin=0, vmax=255)

        return result, rot_mat

    def get_cropped_strobo_template(image):
        """Extracts a rectangular part of the template image defined by cropzone parameters.

        Parameters
        ----------
        image : np.array
            image read by cv imread

        Returns
        -------
        image_cropped : np.array
            pixel values of all pixels inside the cropzone
        global_edge_coords : list[int]
            pixel coordinates of the croprectangle corners
        image_whitecropped : np.array
            original image set to white except for the cropped window. (useful for overlay testing)
        """

        # crop idx bot top left right
        # edge pixels are included in cropped picture
        cropzone = [460, 1050, 1050, 1530]  # [460, 1050, 1050, 1830]

        # adjusting for python not including endindex
        # cropzone[1] = cropzone[1] + 1
        # cropzone[3] = cropzone[3] + 1
        # height_orig, width_orig = image.shape

        image_cropped = image[cropzone[0] : cropzone[1], cropzone[2] : cropzone[3]]
        height_cropped, width_cropped = image_cropped.shape

        image_whitecropped = copy.deepcopy(image)
        image_whitecropped[0 : cropzone[0], :] = np.array(255).astype(np.uint8)
        image_whitecropped[cropzone[1] :, :] = np.array(255).astype(np.uint8)
        image_whitecropped[:, cropzone[3] :] = np.array(255).astype(np.uint8)
        image_whitecropped[:, 0 : cropzone[2]] = np.array(255).astype(np.uint8)

        rect = patches.Rectangle(
            [cropzone[1], cropzone[0]],
            width_cropped,
            height_cropped,
            0,
            fill=False,
            edgecolor="r",
        )  # positive angle for counterclockwise rotation
        # top right, top left, bot left, botright
        corners = rect.get_patch_transform().transform([(0, 0), (1, 0), (1, 1), (0, 1)])
        global_edge_coords = corners

        # fig, ax = plt.subplots(1, 2)
        # ax[0].imshow(image_whitecropped, cmap="gray", vmin=0, vmax=255)
        # ax[1].imshow(image_cropped, cmap="gray", vmin=0, vmax=255)
        # ax[0].add_patch(rect)

        return image_cropped, global_edge_coords, image_whitecropped

    def shift_imageby_warp_matrix(image, warp_matrix) -> np.array:
        """Shifts an image based on a warp matrix defined by the cv2 (cv.warpAffine) package.

        Parameters
        ----------
        image : np.array
            image read by cv imread
        warp_matrix : np.array
            warpmatrix defined by cv2 package

        Returns
        -------
        transformed_img : np.array
            warps an image but keeps the canvas (axes) fixed.
        """
        transformed_img = cv.warpAffine(
            image, warp_matrix, (image.shape[1], image.shape[0])
        )
        return transformed_img

    def run_affine_matching_from_pathfiles(reference_path, im_path):
        """runs affine matching using pathfiles as input.

        Parameters
        ----------
        reference_path : str
            path to reference picture where template is generated from
        image_path : str
            image in which the match should be found

        Returns
        -------
        _aligned_image : np.array
            transformed image, aligned to reference image
        warp_mat : np.array
            warp matrix for cv affine transformation
        """

        # Read the images to be aligned
        im_ref = cv.imread(reference_path)
        im_trans = cv.imread(im_path)

        im_ref_grey = cv.cvtColor(im_ref, cv.COLOR_BGR2GRAY)
        im_shift_grey = cv.cvtColor(im_trans, cv.COLOR_BGR2GRAY)

        (
            im_ref_grey,
            img_ref_global_coords,
            ref_image_whitecropped,
        ) = get_cropped_strobo_template(im_ref_grey)

        _aligned_image, warp_mat = template_matched_affine_transformation(
            im_ref_grey, im_shift_grey, img_ref_global_coords, ref_image_whitecropped
        )

        fig, ax = plt.subplots(1, 1)
        ax.imshow(ref_image_whitecropped, cmap="gray")
        ax.imshow(_aligned_image, cmap="gray", alpha=0.3)
        fig.tight_layout()

        return _aligned_image, warp_mat

    def template_matched_affine_transformation(
        im_ref_grey: np.array,
        im_shift_grey: np.array,
        img_ref_global_coords: np.array,
        ref_image_whitecropped: np.array,
        init_angle: float = -10,
        end_angle: float = 10,
        delta_rough: int = 2,
        steps_fine: float = 0.1,
    ) -> np.array:
        """Adaptive rotation invariant matched template algorithm
        This algorithm takes a reference greyscale image and an shifted image which should be aligned.
        The reference image has significant morphological featue overlap with the shifted image and has smaller dimensions than the shifted image.
        The algorithm finds the template in the image including rotational differences.
        The shifted image is then shifted to match the reference template.
        The init angle and end angle define the range of rotation angles that the shifted image is estimated to lie within.
        Should this be unknown set to -180, 180 or 0, 360. This will come at the cost of increased runtime.
        Should a finer estimation be needed the parameter steps_fine can be decreased to infer greater rotational accuracy.

        #TODO
        #upsampling before running algo - could increase
        #check Translation part for errors
        #make curvefitting algorithm work
        #add scaling option (function already exists, integrate in algorithm)

        Parameters
        ----------
        im_ref_grey : np.array
            reference template
        im_shift_grey : np.array
            image that is rotated towards reference image
        img_ref_global_coords : np.array
            global pixel position of the reference image in the original picture it was taken from
        init_angle : float, optional
            start of range of rotation angles that the shifted image is estimated to lie within, by default -10
        end_angle : float, optional
            end of range of rotation angles that the shifted image is estimated to lie within, by default 10
        delta_rough : int, optional
            stepsize within rotation range for rough angle estimation, by default 2
        steps_fine : float, optional
            final angle estimation accuracy, by default 0.01

        Returns
        -------
        transformed_img : np.array
            image shifted and aligned to match reference template

        Examples
        --------
        >>> REF_PATH = Path("Referenz_Bild_Warm.jpg")
        >>> IMAGE_PATH = Path("alignment_test_data/270_001109.jpg")
        >>> aligned_image, warp_mat = run_affine_matching_from_pathfiles(REF_PATH.as_posix(), IMAGE_PATH.as_posix())
        """

        def rotation_invariant_template_matching(
            im_ref_gray, im_trans_gray, angles, method=cv.TM_CCORR_NORMED
        ):
            result_pnts = []
            height, width = im_ref_gray.shape
            for angle in angles:
                rotated_template, _ = rotate_image_general(im_ref_gray, angle)
                matched_points = cv.matchTemplate(
                    im_trans_gray, rotated_template, method
                )
                min_val, max_val, min_loc, max_loc = cv.minMaxLoc(matched_points)

                # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
                # points are in x,y format while image is in y,x format! However, hight increases towards -y -> thus +hight instead of minus to get to lower pixels
                if method in [cv.TM_SQDIFF, cv.TM_SQDIFF_NORMED]:
                    top_left = min_loc
                else:
                    top_left = max_loc
                top_right = [top_left[0] + width, top_left[1]]
                bottom_right = [top_left[0] + width, top_left[1] + height]
                bottom_left = [top_left[0], top_left[1] + height]
                corners_manual = [top_left, top_right, bottom_right, bottom_left]
                corners_center = [top_left[0] + (width / 2), top_left[1] + (height / 2)]

                corners_rot_man = np.array(
                    [
                        rotate_matrix(
                            c[0],
                            c[1],
                            angle,
                            corners_center[0],
                            corners_center[1],
                            units="DEGREES",
                        )
                        for c in corners_manual
                    ]
                )

                # rect = patches.Rectangle(
                #     bottom_left, width, -height, angle
                # )  # positive angle for counterclockwise rotation
                # corners_rot_rect = rect.get_patch_transform().transform(
                #     [(0, 0), (1, 0), (1, 1), (0, 1)]
                # )

                # corners_rot_man_check = [
                #     rotate_matrix(
                #         c[0],
                #         c[1],
                #         angle,
                #         bottom_left[0],
                #         bottom_left[1],
                #         units="DEGREES",
                #     )
                #     for c in corners_manual
                # ]

                corners = corners_rot_man  # corners_rot_rect  # corners_rot_man

                # fig, ax = plt.subplots(3, 1)
                # ax[0].imshow(matched_points, cmap="gray")
                # ax[1].imshow(rotated_template, cmap="gray")
                # ax[2].imshow(im_trans_gray, cmap="gray")
                # ax[0].add_patch(copy.copy(rect))
                # ax[2].add_patch(rect)
                # ax[2].plot(
                #     corners_center[0], corners_center[1], "r*", linewidth=5, markersize=10
                # )

                result_pnts.append(
                    [min_val, max_val, top_left, bottom_right, angle, corners]
                )

            # find angle with max correlation
            max_cor_idx = np.argmax([el[1] for el in result_pnts])
            max_cor_angle = result_pnts[max_cor_idx][4]
            corners_bbox_max_angle = result_pnts[max_cor_idx][5]
            print(f"The maximum correlation was found at angle: {max_cor_angle}")
            return max_cor_angle, corners_bbox_max_angle

        # values
        height_cropped, width_cropped = im_ref_grey.shape

        # rough angle estimation
        steps_rough = round((end_angle - init_angle) / delta_rough + 1)
        angles = np.linspace(init_angle, end_angle, steps_rough)
        max_cor_angle_rough, _ = rotation_invariant_template_matching(
            im_ref_grey, im_shift_grey, angles
        )

        # finetuned template matching
        max_cor_angle_fine = np.linspace(
            round(max_cor_angle_rough - (delta_rough / 2)),
            round(max_cor_angle_rough + (delta_rough / 2)),
            round((delta_rough / steps_fine) + 1),
        )

        _, corners = rotation_invariant_template_matching(
            im_ref_grey, im_shift_grey, max_cor_angle_fine
        )

        # fig, ax = plt.subplots(1, 2)
        # ax[0].imshow(im_shift_grey, cmap="gray")
        # ax[0].plot([c[0] for c in corners], [c[1] for c in corners], "-g")
        # ax[1].imshow(ref_image_whitecropped, cmap="gray")
        # fig.tight_layout()

        # warping image back to reference image bevore cropping
        warp_mat_fine = cv.getAffineTransform(
            np.float32(corners[[1, 0, 3]]),
            np.float32(img_ref_global_coords[[1, 0, 3]]),
        )
        transformed_img = cv.warpAffine(
            im_shift_grey,
            warp_mat_fine,
            (im_shift_grey.shape[1], im_shift_grey.shape[0]),
        )

        fig, ax = plt.subplots(1, 1)
        ax.imshow(ref_image_whitecropped, cmap="gray")
        ax.imshow(transformed_img, cmap="gray", alpha=0.5)
        fig.tight_layout()

        # cv.imwrite("alignment_test.png", transformed_img)
        print("Done")

        return transformed_img, warp_mat_fine

    # Rotating images to given Degrees
    def Rotate_Images(image):

        """Function rotating each image within specified analysis range for Pre_Amp analysis (from First_Image to Last_Image), only for angles 270 & 90
           only runs if Rotate == "yes" 
           images are rotated -9° (or 9° clockwise) with center in center of image
           this transformation is applied in order to make the meniscus of the fluid within the left Pre_Amp (Pre_Amp_2) as horizontal as possible
           without this trasformation, later found grayscale values would be irrelevant

        Parameters
        ----------
        image : string
            path of every image to be analyzed in Pre_Amp analysis

        Returns
        -------
        image : string
            path of every image to be analyzed in Pre_Amp analysis
            this path now displays the image after transformation
        """

        if Rotate == "yes":
            img = cv.imread(image, cv.IMREAD_GRAYSCALE)
            img_center = [el / 2 for el in img.shape]
            rot_angle = -9
            img_rot_mat = cv.getRotationMatrix2D((img_center), rot_angle, 1)
            img_rot = cv.warpAffine(img, img_rot_mat, (img.shape[1], img.shape[0]))

            cv.imwrite(image, img_rot)

        return image

    # Renaming every image from 0 onwards, checking only .jpg
    def Image_renamer():

        """Function renaming each image within specified analysis range for Pre_Amp analysis (from First_Image to Last_Image), only for angles 270 & 90
           only runs if Rename == "yes" 
           original image names are deleted and renamed using the format "Bild + count of image", starts with Bild 0
           this function needs to run because cv.imread function cannot access files with ° contained in their name
           creates a Run_Checker to check afterwards if this function ran or not

        Parameters
        ----------
        Folder_Path (global): string
            path of every strobo run folder
            this path is identical to Raw_Path, but changes if folder has been renamed 

        Returns
        -------
        Rename_checker : boolean
            True == Image_renamer ran successfully
            False == Image_renamer didn't run successfully or was never called

        renamed_list_of_images : list of strings
            creating and returning a new list, containing all new paths of renamed images to be analyzed for Pre_Amp analysis
        """
                
        Rename_count = 0
        renamed_list_of_images = []

        if Rename == "yes":
            Rename_checker = True

            # taking only jpg files and renaming them numerically using Rename_count
            for image in glob.iglob(os.path.join(Folder_Path, "*.jpg")):
                os.chdir(Folder_Path)
                image_name, image_type = os.path.splitext(image)
                image_name = str("Bild " + str(Rename_count))
                Rename_count += 1
                new_image = "{}{}".format(image_name, image_type)
                os.rename(image, new_image)
                renamed_list_of_images.append(new_image)

            renamed_list_of_images = sorted(
                renamed_list_of_images, key=lambda f: float(re.findall("(\d+)", f)[0])
            )

        else:
            Rename_checker = False

        return Rename_checker, renamed_list_of_images

    # Checking dimensions for (First_Image to Last_Image), not adding "empty" images, forming a final list (Pre_Amp_Images)
    # Also analyzing only images from First_Image to Last_Image
    def Dimension_Checker():

        """Function checking if the dimension of analyzed images is correct (== 1936x1216)
           images with incorrect dimensions will be appended to Inacessible_Images list and won't be further analyzed
           images with correct dimensions will be appended to Pre_Amp_Images list and will be further analyzed 
           if Rename_checker == "True"  it analyzes images from the list with renamed paths, if not, analyzes image from the original list

        Parameters
        ----------
        Rename_checker : boolean
            True == Image_renamer ran successfully
            False == Image_renamer didn't run successfully or was never called

        renamed_list_of_images : list of strings
            new list, containing all new paths of renamed images to be analyzed for Pre_Amp analysis

        Folder_Path (global): string
            path of every strobo run folder
            this path is identical to Raw_Path, but changes if folder has been renamed 

        First_Image (global): integer
            input by user before starting SARA
            used to find the image with that count within angle folder (only folders 270 and 90)
            first image to be analyzed for Pre_Amp analysis

        Last_Image (global): integer
            input by user before starting SARA
            used to find the image with that count within angle folder (only folders 270 and 90)  
            last image to be analyzed for Pre_Amp analysis

        list_of_images_sorted: list of strings
            sorted list of originally-named images
            list sorts names in ascending order


        Returns
        -------
        yKoordinates : list of integers
            list of integers from 0 to 381 (length of Cropped_zone, y=0 to y=381)
            this list will be used for the y axes in plots

        Pre_Amp_Images : list of strings
            list containing paths of images to be analyzed

        Pre_Amp_Image_counter : list of integers
            a created list appending counters of Pre_Amp_Images starting from 0

        Inaccessible_Images : list of strings
            list containing paths of images to not* be analyzed, because they dont have the required dimensions
        """
                
        Rename_checker, renamed_list_of_images = Image_renamer()

        Image_count = 0
        yKoordinates = list(
            reversed(range(y, y + 381))
        )  # creating a 0 to 381 y koordinates for the Cropped_Zone and reversing it to 381 to 0

        if Rename_checker == True:
            for image in renamed_list_of_images:
                Image_count += 1
                os.chdir(Folder_Path)

                if Image_count in range(First_Image, Last_Image):
                    Pre_Amp_Image_counter.append(Image_count)

                    image_Path_meta = os.path.join(Folder_Path, image)
                    image_meta = Image.open(image_Path_meta)
                    image_meta.close()
                    image_meta_string = str(image_meta)
                    image_meta_string_split = image_meta_string.split()

                    if (
                        "size=1936x1216" in image_meta_string_split
                    ):  # x=1936 (width) and y=1216 (height)
                        Pre_Amp_Images.append(image)

                    else:
                        Inaccessible_Images.append(image)
        else:
            for image in list_of_images_sorted:
                Image_count += 1
                os.chdir(Folder_Path)

                if Image_count in range(First_Image, Last_Image):
                    Pre_Amp_Image_counter.append(Image_count)

                    image_Path_meta = os.path.join(Folder_Path, image)
                    image_meta = Image.open(image_Path_meta)
                    image_meta.close()
                    image_meta_string = str(image_meta)
                    image_meta_string_split = image_meta_string.split()

                    if "size=1936x1216" in image_meta_string_split:
                        Pre_Amp_Images.append(image)

                    else:
                        Inaccessible_Images.append(
                            image
                        )  # removing inaccessible images from the PreAmp list

        # Removing first elements by the len(inacessible_images) from y Koordinates for the graph. This variable doesn't get outputted anymore, but it can be useful
        yKoordinates = yKoordinates[len(Inaccessible_Images) :]

        return Pre_Amp_Images, Pre_Amp_Image_counter

    # Forming arrays with cropped zones of image and finding grayscale averages for each row of pixels
    def Area_Grayscale(processed_image):

        """Function checking if the dimension of analyzed images is correct (== 1936x1216)
           images with incorrect dimensions will be appended to Inacessible_Images list and won't be further analyzed
           images with correct dimensions will be appended to Pre_Amp_Images list and will be further analyzed 
           if Rename_checker == "True"  it analyzes images from the list with renamed paths, if not, analyzes image from the original list
           returns 3 lists with averages. these lists will be used for Meniscus_Optimizer

        Parameters
        ----------
        processed_image : np.ndarray
            numpy array of analyzing image 

        Folder_Path (global): string
            path of every strobo run folder
            this path is identical to Raw_Path, but changes if folder has been renamed 

        Returns
        -------
        averages_1 : list of floats
            list of averages of grayscale values for each row (of 1 pixel height) from the Cropped_Zone (0 to 381)
            this list analyzes values from Cropped_Zone_1

        averages_2 : list of floats
            list of averages of grayscale values for each row (of 1 pixel height) from the Cropped_Zone (0 to 381)
            this list analyzes values from Cropped_Zone_2

        averages_3 : list of floats
            list of averages of grayscale values for each row (of 1 pixel height) from the Cropped_Zone (0 to 381)
            this list analyzes values from Cropped_Zone_3
        """
                
        averages_1 = []
        averages_2 = []
        averages_3 = []

        # cropped zones are next to each other, starting from 1 on the very left
        Cropped_Zone_1 = processed_image[int(y) : int(y + 381), int(x) - 14 : int(x)]
        Cropped_Zone_2 = processed_image[int(y) : int(y + 381), int(x) : int(x) + 14]
        Cropped_Zone_3 = processed_image[int(y) : int(y + 381), int(x) + 14 : int(x) + 28]

        # For each y calculating x averages for each cropped zone
        for Y_Koordinate in Cropped_Zone_1:
            Rows = [row[1] for row in Y_Koordinate]
            Average_in_row = (
                sum(Rows) / 14
            )  # averages by 14 because cropped zones contain an area x to x+14
            averages_1.append(Average_in_row)

        for Y_Koordinate in Cropped_Zone_2:
            Rows = [row[1] for row in Y_Koordinate]
            Average_in_row = sum(Rows) / 14
            averages_2.append(Average_in_row)

        for Y_Koordinate in Cropped_Zone_3:
            Rows = [row[1] for row in Y_Koordinate]
            Average_in_row = sum(Rows) / 14
            averages_3.append(Average_in_row)

        return averages_1, averages_2, averages_3

    # Calculating derivative of a  1d-array and cleans up the edges of the array
    def Process_Image(array):

        """Function performing derivation from a 1d-array (in this case, a list of floats)
           finding derivatives of grayscale value averages will find the biggest change between two pixels within Cropped_Zone coordinates
           the biggest value from the returned list will later be picked to identify plausible meniscus position 

        Parameters
        ----------
        array : 1d-np.ndarray
            a list of floats
            in this script, the lists used are: averages_1, averages_2 and averages_3
             
        Returns
        -------
        processed_array : 1d-np.ndarray
            a list of derivated values
        """
                
        # Smoothing
        # array = np.convolve(array,[0.5,1,0.5], 'same')
        processed_array = np.gradient(array, 3)
        # Clean up edge artefacts
        processed_array[0] = 0
        processed_array[1] = 0
        processed_array[-1] = 0
        processed_array[-2] = 0

        return processed_array

    # Finding best meniscus out of 3 cropped zones
    def Meniscus_Optimizer(average_1, average_2, average_3):
        global average
        global ROI_Epsilon  # accessing global ROI_Epsilon and gaining control

        """Function analyzing the 3 lists of derivated grayscale value averages and finding the best plausible meniscus position using simple statistics
           it uses 2 Epsilons (Epsilon & ROI_Epsilon) as ranges to accept or discard found meniscus values
           Epsilon is an integer and in combination with the boolean "In_RangeA1_A2" checks found meniscii from the 3 Cropped_Zones are almost identical or not
           Example: if average_1 = 511, average_2 = 512 & average_3 = 811, then here average_3 will be discarded  

        Parameters
        ----------
        ROI_Epsilon (global): integer
            arbitrary region of interest, which defines a range of pixels
            starts as 15
            in case the next found meniscus is further than the defined range from the previous meniscus, then that found meniscus is discarded in favor of other values
            EXCEPTION: if average < 305 and meniscus still outside range, then ROI_Epsilon == 70
            this value restricts found meniscii using logical flow of the fluid, thus emitting too high or too low poisitions of meniscus

        average_1 : intp
            the biggest derivated value (== np.argmax(list)) from the grayscale values of Cropped_Zone_1 

        average_2 : intp
            the biggest derivated value (== np.argmax(list)) from the grayscale values of Cropped_Zone_2

        average_3 : intp
            the biggest derivated value (== np.argmax(list)) from the grayscale values of Cropped_Zone_3

        Returns
        -------
        average (global): integer
            the best plausible meniscus position (Y Coordinate)
            this value will then be appended to a list and then plotted to show the position of meniscii during the run
        """

        average_list = []  # list with original average_1, average_2, average_3
        remove_average_list = []  # list of all averages that do not fit the defined ROI
        new_average_list = (
            []
        )  # list with statistic-transformed values if they are within epsilon
        Epsilon = 4  # Epsilon used here as a kind of a st.dev to compare 2 averages
        In_RangeA1_A2 = (
            False  # Boolean used to check if all 3 comparisons are True or False
        )

        average_list.extend([average_1, average_2, average_3])

        for average_x in average_list:

            if (int(average_x) >= int(average) - ROI_Epsilon) and (
                int(average_x) <= int(average)
            ):
                pass
                # In_RangeA1_A2 = False
            else:
                remove_average_list.append(average_x)

        average_list = [
            item for item in average_list if item not in remove_average_list
        ]  # removing all averages that do not fit the defined ROI
        list_length = len(average_list)

        # if there is more than 1 average in the [original list - [elements that are not within defined ROI]]
        if list_length > 1:
            boolean_list = []
            ROI_Epsilon = 15

            for a, b in itertools.combinations(average_list, 2):
                if (a >= b - Epsilon) and (a <= b + Epsilon):
                    In_RangeA1_A2 = True
                    boolean_list.append(In_RangeA1_A2)

                    new_average = (a + b) / 2
                    new_average_list.append(new_average)

                else:
                    In_RangeA1_A2 = False
                    boolean_list.append(In_RangeA1_A2)

            if any(boolean_list) == False:
                average_list.sort()
                average = average_list[0]

            else:  # if all boolean are False = if all 3 averages are very different from one another
                new_average_list.sort()
                average = new_average_list[0]

        elif list_length == 1:  # if there is only one average within defined ROI
            average = average_list[0]
            ROI_Epsilon = 15

        else:  # if all averages from original list are very far from previous meniscus
            average = average

            if average < (80 * 381) / 100:  # or < 305
                ROI_Epsilon = 70  # changing ROI Epsilon to a greater value so it can catch up with the next meniscus. only happens from meniscus higher than 80% of length Cropped_Zone
            else:
                ROI_Epsilon = 15

        return average

    # Frank's function to change pixel to volume
    def preAmpFilltoVolume(fill_level_percentage):

        """Frank Schwemmer's function converting the fill_level_percentage to volume in mikroliters
           for this function, to find fill_level_percentage values, the found meniscii y coordinates are inserted into the following formula: 
           =(A-C)/(A-B) where A is y coordinate of highest point of PreAmp chamber, B is y coordinate of lowest point and C is y coordinate of found Meniscus
           this value is then taken as a parameter in this function   

        Parameters
        ----------
        fill_level_percentage: float
            float representing the percentage of filled Pre_Amp chamber

        Returns
        -------
        volume: float
            volume of filled Pre_Amp chamber in mikroliters
        """

        # radius associated to volume in array vol
        r_vol = [
            46,
            46.1,
            46.2,
            46.3,
            46.4,
            46.5,
            46.6,
            46.7,
            46.8,
            46.9,
            47,
            47.1,
            47.2,
            47.3,
            47.4,
            47.5,
            47.6,
            47.7,
            47.8,
            47.9,
            48,
            48.1,
            48.2,
            48.3,
            48.4,
            48.5,
            48.6,
            48.7,
            48.8,
            48.9,
            49,
            49.1,
            49.2,
            49.3,
            49.4,
            49.5,
            49.6,
            49.7,
            49.8,
            49.9,
            50,
            50.1,
            50.2,
            50.3,
            50.4,
            50.5,
            50.6,
            50.7,
            50.8,
            50.9,
            51,
            51.1,
            51.2,
            51.3,
            51.4,
            51.5,
            51.6,
            51.7,
            51.8,
            51.9,
            52,
            52.1,
            52.2,
            52.3,
            52.4,
            52.5,
            52.6,
            52.7,
            52.8,
            52.9,
            53,
            53.1,
            53.2,
            53.3,
            53.4,
            53.5,
            53.6,
            53.7,
            53.8,
            53.9,
            54,
            54.1,
            54.2,
            54.3,
            54.4,
            54.5,
            54.6,
            54.7,
            54.8,
            54.9,
            55,
            55.1,
            55.2,
            55.3,
            55.4,
            55.5,
            55.6,
            55.7,
            55.8,
            55.9,
            56,
            56.1,
            56.2,
            56.3,
            56.4,
            56.5,
            56.6,
            56.7,
            56.8,
            56.9,
            57,
            57.1,
            57.2,
            57.3,
            57.4,
            57.5,
            57.6,
            57.7,
            57.8,
            57.9,
            58,
            58.1,
            58.2,
            58.3,
            58.4,
            58.5,
            58.6,
            58.7,
            58.8,
            58.9,
            59,
            59.1,
            59.2,
            59.3,
            59.4,
            59.5,
            59.6,
            59.7,
            59.8,
            59.9,
            60,
            60.1,
            60.2,
            60.3,
            60.4,
            60.5,
            60.6,
            60.7,
            60.8,
            60.9,
            61,
            61.1,
            61.2,
            61.3,
            61.4,
            61.5,
            61.6,
            61.7,
            61.8,
        ]
        # volume for given radius of r_vol
        vol = [
            107.5166896,
            107.5156149,
            107.5145404,
            107.5134661,
            107.5123919,
            107.5113179,
            107.510244,
            107.5091694,
            107.5080947,
            107.5068072,
            107.5048484,
            107.4995279,
            107.4643019,
            107.3673482,
            107.2060598,
            106.9932543,
            106.7446333,
            106.4731369,
            106.1855378,
            105.8861317,
            105.5777508,
            105.2620613,
            104.9405269,
            104.6144965,
            104.2852692,
            103.9541467,
            103.6224246,
            103.2907213,
            102.9590624,
            102.6274475,
            102.2958763,
            101.9643487,
            101.6328642,
            101.3014226,
            100.9700236,
            100.638667,
            100.3073524,
            99.97607966,
            99.64484846,
            99.31365852,
            98.9825096,
            98.65140143,
            98.32033375,
            97.9893063,
            97.65831884,
            97.3273711,
            96.99646293,
            96.6655939,
            96.33476385,
            96.00397254,
            95.67321973,
            95.34250517,
            95.01182864,
            94.6811899,
            94.35058871,
            94.02002484,
            93.68949806,
            93.35900814,
            93.02855486,
            92.69813799,
            92.36775712,
            92.03738389,
            91.70677139,
            91.37597882,
            91.04498286,
            90.71274112,
            90.37833405,
            90.04062401,
            89.6980163,
            89.34963895,
            88.99396753,
            88.62952223,
            88.25474151,
            87.8671683,
            87.46318858,
            87.03670818,
            86.58023166,
            86.09168508,
            85.57145256,
            85.01980742,
            84.43705567,
            83.82348324,
            83.17939076,
            82.50504084,
            81.80069579,
            81.06660849,
            80.30302292,
            79.51017463,
            78.68829121,
            77.83759266,
            76.9582918,
            76.0506007,
            75.11523002,
            74.15465818,
            73.17226933,
            72.17072276,
            71.15198943,
            70.11762179,
            69.06890261,
            68.00692379,
            66.93263911,
            65.84689845,
            64.75047126,
            63.6440638,
            62.52833193,
            61.4038909,
            60.27132309,
            59.13118436,
            57.98400904,
            56.83031432,
            55.67060407,
            54.50537204,
            53.33510488,
            52.16027099,
            50.98108975,
            49.79756824,
            48.60970511,
            47.41748203,
            46.22089662,
            45.01997147,
            43.81470673,
            42.60510251,
            41.39115894,
            40.17287616,
            38.95025428,
            37.72329432,
            36.49202724,
            35.25643273,
            34.01649962,
            32.77227106,
            31.52409803,
            30.27247158,
            29.01787723,
            27.76082751,
            26.50212314,
            25.24272558,
            23.9836054,
            22.7257459,
            21.47014869,
            20.21783963,
            18.96987554,
            17.72735177,
            16.49141095,
            15.26325345,
            14.04414994,
            12.83545693,
            11.63863668,
            10.45528234,
            9.287152499,
            8.136217271,
            7.004725389,
            5.895303645,
            4.811111608,
            3.756115602,
            2.73560451,
            1.757405429,
            0.844518148,
            0.202977619,
            0,
        ]

        # Give max or min volume, if fill_level is out of bounds.
        if fill_level_percentage >= 1:
            return vol[0]

        if fill_level_percentage <= 0:
            return 0

        # Calculate radius from fill level
        r_outer_edge = 61.8
        r_inner_edge = 46.3
        r = fill_level_percentage * (r_inner_edge - r_outer_edge) + r_outer_edge

        # to find index of first element just
        # greater than r
        res = list(filter(lambda i: i > r, r_vol))[0]
        position = r_vol.index(res)

        # volume at position and position-1
        volume_plus = vol[position]
        volume_minus = vol[position - 1]

        # Linear interpolation of volume
        volume = (
            volume_plus + (volume_plus - volume_minus) * (r - r_vol[position]) / 0.1
        )

        # debug
        # print ("min pos ", r_vol[position-1],"max pos ", r_vol[position], " min pos ", r, " volume @ min pos ", vol[position-1], " volume @ max pos ", vol[position])

        # calculate volume from radius and return volume
        return volume

    # Runs the full code for Pre_Amp analysis, creating a list with all positions of meniscus
    def Pre_Amp_analysis():
        global Pre_Amp_Image_counter
        global combined_results
        global image_Path

        """Function containing and running all the functions relevant for the left Pre_Amp chamber (or Pre_Amp_2) 
           only runs if Rename == yes 
           displays a preview window named "Analyzing_Images" showing the position of found meniscii in analyzed images in real time
           this preview window is closed automatically after the analysis finishes
           saves a plot in target_Folder with positions of found meniscii (y axis) for analyzed PreAmp images (x axis)

        Parameters
        ----------
        Pre_Amp_Image_counter (global): 
            a created list appending counters of Pre_Amp_Images starting from 0

        image_Path (global): string
            path of analyzing image

        combined_results (global): list of integers
            list containing all the found meniscii position (their y coordinate) during the run

        Rename (global): string
            this string == "yes" (if images need to be renamed) or "no" (if not)

        Folder_Path (global): string
            path of every strobo run folder
            this path is identical to Raw_Path, but changes if folder has been renamed 

        processed_image : np.ndarray
            numpy array of analyzing image

        Returns
        -------
        real_positions_of_meniscus : list of integers
            list of  all found meniscii during the run, but adjusted so that y = global y and not local defined by the Cropped_Zone
            Formula =>  = meniscus + y (of defined Cropped_Zone) for meniscus in positions_of_meniscus

        combined_results (global): list of tuples
            list of tuples formed by zipping real_position_of_meniscus and Pre_Amp_Images

        combined_results_vol : list of tuples
            list of tuples formed by zipping real_positions_vol and Pre_Amp_Images

        Part_1_checker : boolean
            if combined_results and combined_results_vol was calculated correctly then Part_1_checker = True
            otherwise Part_1_checker = False
        """

        Pre_Amp_Images, Pre_Amp_Image_counter = Dimension_Checker()
        window_name = "Analyzing_Images"  # preview window name

        if Rename == "yes":
            image_Path_pre = os.path.join(Folder_Path, Pre_Amp_Images[0])
            aligned_im_test, warp_mat = run_affine_matching_from_pathfiles(
                reference_path, image_Path_pre
            )

        # im_ref = cv.imread(reference_path, cv.IMREAD_GRAYSCALE)
        # fig, ax = plt.subplots(1, 1)
        # ax.imshow(im_ref, cmap="gray")
        # rect = patches.Rectangle([x, y], 14, 381)
        # ax.add_patch(rect)
        # ax.imshow(aligned_im_test, cmap="gray", alpha=0.3)
        # fig.tight_layout()

        for image in Pre_Amp_Images:
            image_Path = os.path.join(Folder_Path, image)

            if Rename == "yes":
                im_shift = cv.imread(image_Path, cv.IMREAD_GRAYSCALE)
                _aligned_image = shift_imageby_warp_matrix(im_shift, warp_mat)

                cv.imwrite(os.path.basename(image_Path), _aligned_image)
            image = Rotate_Images(image)

            processed_image = cv.imread(image_Path)

            # calculating argmax after derivation for all 3 accumulated averages from the 3 different cropped zones
            averages_1, averages_2, averages_3 = Area_Grayscale(processed_image)

            average_1 = Process_Image(averages_1)
            average_1 = np.argmax(average_1)

            average_2 = Process_Image(averages_2)
            average_2 = np.argmax(average_2)

            average_3 = Process_Image(averages_3)
            average_3 = np.argmax(average_3)

            # Running Meniscus_Optimizer
            average = Meniscus_Optimizer(average_1, average_2, average_3)

            positions_of_meniscus.append(average)

            # Drawing the Crop_Zone on the shown window, displaying a window with image analysis real-time
            cv.rectangle(
                img=processed_image,
                pt1=(x, y),
                pt2=(x + 14, y + 381),
                color=(0, 0, 255),
                thickness=1,
            )
            cv.line(
                processed_image,
                (x, y + int(average)),
                (x + 14, y + int(average)),
                (125, 250, 0),
                thickness=2,
            )
            cv.imshow(window_name, processed_image)
            cv.waitKey(8)  # cv.waitKey(8)

        # Converting relative y position to absolute
        real_positions_of_meniscus = [
            position + y for position in positions_of_meniscus
        ]

        # Median filter with 3 values
        real_positions_of_meniscus = ndimage.median_filter(
            real_positions_of_meniscus, size=3
        )

        # Transforming found meniscii y coordinates to % and then to volume
        real_positions_percentage = [
            ((1134 - position) / (1134 - 682))
            for position in real_positions_of_meniscus
        ]  # = (A-C)/(A-B) where A is highest point of PreAmp chamber, B lowest point and C is global y of found Meniscus
        real_positions_vol = [
            preAmpFilltoVolume(percentage) for percentage in real_positions_percentage
        ]

        # Removing first elements by the len(inacessible_images) from y Koordinates for the graph
        Pre_Amp_Image_counter = Pre_Amp_Image_counter[len(Inaccessible_Images) :]

        # Plots positions of meniscus for all PreAmp images, saving plot in target_Folder
        plt.plot(Pre_Amp_Image_counter, real_positions_of_meniscus)
        plt.gca().invert_yaxis()
        plt.xlabel("Image Nr.")
        plt.ylabel("Position of Meniscus (Y-inverted)")
        plt.title("Meniscus Positions in the Folder")
        plt.savefig(
            target_Folder
            + "\Positions of Meniscus "
            + str(os.path.basename(Folder_Path))
            + ".png",
            dpi=500,
        )

        plt.ion()
        plt.show()
        plt.close()

        # Plots volume for all PreAmp images, saving plot in target_Folder
        plt.plot(Pre_Amp_Image_counter, real_positions_vol)
        plt.xlabel("Image Nr.")
        plt.ylabel("Pre_Amp2 volume")
        plt.title("Volume levels in the Folder")
        plt.savefig(
            target_Folder
            + "\Pre_Amp2 volume "
            + str(os.path.basename(Folder_Path))
            + ".png",
            dpi=500,
        )

        plt.ion()
        plt.show()
        plt.close()

        # Combining results, destroying preview window
        combined_results = list(zip(Pre_Amp_Images, real_positions_of_meniscus))
        combined_results_vol = list(zip(Pre_Amp_Images, real_positions_vol))
        cv.destroyWindow(window_name)
        Part_1_checker = True

        return (
            real_positions_of_meniscus,
            Part_1_checker,
            combined_results,
            combined_results_vol,
        )

    (
        real_positions_of_meniscus,
        Part_1_checker,
        combined_results,
        combined_results_vol,
    ) = Pre_Amp_analysis()

    # Finding image with max volume and max volume in mikroL
    def find_max_volume(combined_results):

        """Function finding image containing maximum filled Pre_Amp Chamber (using the meniscus for left chamber)
           this function doesn't save the images, only extract image name and the corresponding fluid meniscus position
           images are only saved later in an outer For Loop after both find_max_volume() &  save_PreAmp_images() functions have been called

        Parameters
        ----------
        Pre_Amp_Image_counter (global): 
            a created list appending counters of Pre_Amp_Images starting from 0

        number_image_95 (global): integer
            count of the found image containing meniscus at 95 mikroLiter filled position
            Example: 312 

        image_95_meniscus (global): string
            found meniscus for fluid at 95 mikroLiter filled position
            this string will later be converted to int

        First_Image (global): 
            input by user before starting SARA
            used to find the image with that count within angle folder (only folders 270 and 90)
            first image to be analyzed for Pre_Amp analysis

        combined_results (global): list of integers
            list containing all the found meniscii position (their y coordinate) during the run

        Returns
        -------
        image_max_number : string
            contains the name of image containing maximum filled Pre_Amp Chamber (without the ".jpg")
            Example: "Bild 301"

        image_max_num : string
            contains the name of image containing maximum filled Pre_Amp Chamber (with the ".jpg")
            Example: "Bild 301.jpg"
        
        max_volume (global): string
            contains found meniscus for fluid at maximum filled Pre_Amp Chamber
            this string will later be converted to int
        """

        # extracting images only from 95 mikroL and afterwards
        number_image_list = combined_results[number_image_95 - First_Image + 1 :]
        # finding image with minimal y = maximal volume
        image_max = min(number_image_list, key=lambda g: g[1])
        image_max_num = image_max[0]  # taking only image name = image count
        temporary = image_max_num.split(".")  # splitting .jpg
        image_max_number = temporary[0]  # saving image name = count without the .jpg
        max_volume = image_max[1]  # saving y = meniscus level
        max_volume = (
            (int(image_95_meniscus) - max_volume) * 0.083
        ) + 95  # converting y to volume by using arbitrary 0.083 mirkoL = 1 pixel

        return image_max_number, max_volume, image_max_num

    # Saving image with meniscus at 95 mikroL
    def save_PreAmp_images(combined_results):
        global new_path
        global image_95
        global number_image_95
        global image_95_meniscus

        """Function finding image containing meniscus at 95 mikroLiter filled position (using the meniscus for left chamber)
           also finds image used for asymmetry calculations (y = 932) 
           this function also saves the images in target_Folder

        Parameters
        ----------
        Folder_Path (global): string
            path of every strobo run folder
            this path is identical to Raw_Path, but changes if folder has been renamed 

        combined_results (global): list of integers
            list containing all the found meniscii position (their y coordinate) during the run

        Returns
        -------
        new_path (global): list of strings
            list of all image paths within Folder_Path (angle folder)

        image_95 (global): string
            contains the name of image containing meniscus at 95 mikroLiter filled position (with the ".jpg")
            Example: "Bild 312.jpg"

        number_image_95 (global): integer
            count of the found image containing meniscus at 95 mikroLiter filled position
            Example: 312

        image_95_meniscus (global): string
            found meniscus for fluid at 95 mikroLiter filled position
            this string will later be converted to int

        image_asymmetrie : string
            contains the name of image used for asymmetry calculations
            Example: "Bild 333.jpg"

        new_image_asymmetrie : string
            renamed image_asymmetrie string, used for renaming the image after saving it in target_Folder

        number_image_asymmetrie : integer
            count of the found image containing meniscus at 95 mikroLiter filled position
            Example: 333

        image_asymmetrie_meniscus : string
            found meniscus in image used for asymmetry calculations
            this string will later be converted to int
            will be later renamed to "Pre_Amp2_meniscus"
        """

        new_path = os.listdir(Folder_Path)  # list of all images within a winkel ordner
        os.chdir(Folder_Path)  # changing current directory to the winkel ordner

        # finding y = 702 (95 mikroL) tuple from the list of tuples, also finding y = 932 for Asymmetrie. Both have an acceptance range +- 3
        image_95 = [
            item
            for item in combined_results
            if int(item[1]) == 775
            or int(item[1]) == 776
            or int(item[1]) == 774
            or int(item[1]) == 777
            or int(item[1]) == 773
        ]

        image_asymmetrie = [
            item
            for item in combined_results
            if int(item[1]) == 1037
            or int(item[1]) == 1038
            or int(item[1]) == 1036
            or int(item[1]) == 1039
            or int(item[1]) == 1035
        ]

        # getting tuple cotaining image with 95 mikroL
        image_95 = image_95[0]
        # getting number from image name
        image_95, image_95_meniscus = image_95
        # getting rest of image name
        temporary = image_95.split(" ")
        number_image_95 = temporary[1]
        temporary = number_image_95.split(".")
        number_image_95 = int(temporary[0])

        # getting tuple cotaining y=932
        image_asymmetrie = image_asymmetrie[0]
        # getting number from image name
        image_asymmetrie, image_asymmetrie_meniscus = image_asymmetrie
        # getting rest of image name
        temporary = image_asymmetrie.split(" ")
        number_image_asymmetrie = temporary[1]
        temporary = number_image_asymmetrie.split(".")
        number_image_asymmetrie = int(temporary[0])

        # saving image with 95 mikroL volume (not needed) in PreAmp2 Kammer, renaming it to fit the analyzed folder, also saving asymmetrie image
        for image in glob.iglob(os.path.join(Folder_Path, "*.jpg")):
            # if image_95 in new_path:
            # shutil.copy(image_95, target_Folder)
            # image_95_path = os.path.join(target_Folder, image_95)

            # temporary = image_95.split('.')
            # image_95 = temporary[0]
            # new_image_95 = image_95 + '_' + '95mikro_' + str(os.path.basename(Folder_Path)) + '.png'
            # New_image_95_path = os.path.join(target_Folder, new_image_95)
            # os.rename(image_95_path, New_image_95_path)

            if image_asymmetrie in new_path:
                shutil.copy(image_asymmetrie, target_Folder)
                image_asymmetrie_path = os.path.join(target_Folder, image_asymmetrie)

                temporary = image_asymmetrie.split(".")
                image_asymmetrie = temporary[0]
                new_image_asymmetrie = (
                    image_asymmetrie
                    + "_"
                    + "Asymmetrie_"
                    + str(os.path.basename(Folder_Path))
                    + ".jpg"
                )
                New_image_asymmetrie_path = os.path.join(
                    target_Folder, new_image_asymmetrie
                )
                os.rename(image_asymmetrie_path, New_image_asymmetrie_path)

        return (
            image_asymmetrie,
            new_image_asymmetrie,
            number_image_asymmetrie,
            image_asymmetrie_meniscus,
        )

    (
        image_asymmetrie,
        new_image_asymmetrie,
        number_image_asymmetrie,
        image_asymmetrie_meniscus,
    ) = save_PreAmp_images(combined_results)
    image_max_number, max_volume, image_max_num = find_max_volume(combined_results)

    # Renaming already found image_asymmetrie_meniscus to Pre_Amp2_meniscus and changing it to integer
    global Pre_Amp2_meniscus
    Pre_Amp2_meniscus = int(image_asymmetrie_meniscus)

    # saving image with max volume
    for image in glob.iglob(os.path.join(Folder_Path, "*.jpg")):
        if image_max_num in new_path:
            shutil.copy(image_max_num, target_Folder)
            image_max_path = os.path.join(target_Folder, image_max_num)

            temp = image_max_num.split(".")
            image_max_num = temp[0]
            new_image_max = (
                image_max_num
                + "_"
                + "PreAmp Max_"
                + str(os.path.basename(Folder_Path))
                + ".png"
            )
            New_image_max_path = os.path.join(target_Folder, new_image_max)
            os.rename(image_max_path, New_image_max_path)

    ############################# Step 2 (other chamber, Pre_Amp_1 analysis) #########################################################

    # Custom scipy.ndimage rotation to track coordinates after Rotation, finding img.shape after rotation
    def Rotate_Pre_Amp1(new_image, xy, angle):

        """Function re-rotating the found images (image_95 and image_asymmetrie) which were previously saved in target_Folder

           images are rotated using angle parameter
           this transformation is applied in order to make the meniscus of the fluid within the right Pre_Amp (Pre_Amp_1) as horizontal as possible
           without this trasformation, later found grayscale values would be irrelevant

        Parameters
        ----------
        new_image : string
            path of every image to be rotated

        Analysis_Folder : string
            path of S.A.R.A Analysis folder, identical to target_Folder string, but here renamed so its easier to work with within "Step 2"

        xy : 1d-np.array 
            coordinates of left corner of Cropped_Zone found before image rotation (here using (x2, y2) as coordinates)

        angle : integer
            angle of rotation (+ is counter clockwise, - is clockwise)
        
        Returns
        -------
        img_rot : string
            path of every image to be analyzed in Pre_Amp analysis
            this path now displays the image after transformation

        new_height : integer
            y dimension of the rotated image

        new_width : integer
            x dimension of the rotated image

        (x2, y2) : 1d-np.array 
            new, tracked coordinates of left corner of Cropped_Zone found after image rotation
        """
                
        for new_image in glob.iglob(os.path.join(Analysis_Folder, "*.jpg")):

            img = cv.imread(new_image)
            new_height, new_width, c = img.shape

            img_rot = ndimage.rotate(img, angle)
            org_center = (np.array(img.shape[:2][::-1]) - 1) / 2.0
            rot_center = (np.array(img_rot.shape[:2][::-1]) - 1) / 2.0
            org = xy - org_center
            a = np.deg2rad(angle)
            new = np.array(
                [
                    org[0] * np.cos(a) + org[1] * np.sin(a),
                    -org[0] * np.sin(a) + org[1] * np.cos(a),
                ]
            )

            if angle == 344:
                cv.imwrite(new_image, img_rot)

        return new_height, new_width, img_rot, new + rot_center

    # Finding arg(max) = PreAmp 1 meniscus and calculating Asymmetry
    def Pre_Amp1_analysis(target_Folder):
        global Degrees
        global Rotate
        global x
        global y
        global Analysis_Folder
        global Run_counter
        global Pre_Amp1_meniscus
        global real_Pre_Amp1_meniscus

        """Function finding fluid meniscus image_asymmetrie image for the right Pre_Amp chamber (Pre_Amp1)
           within itself, it runs functions Rotate_Pre_Amp1(), Area_Grayscale_Pre_Amp1(), Process_Image() & preAmpFilltoVolume()
           only runs if Part_1_checker == True
           redefines Cropped_Zone coordinates for the right chamber, using new global x,y and new angle of rotation
           finds Asymmetry value needed for the results
           displays a preview window named "Analyzing_Images" showing the position of found meniscus for the right chamber in analyzed image_asymmetrie image
           this preview window is closed automatically after the analysis finishes
           saves a plot in target_Folder with positions of calculated averages of grayscale values (y axis) for Cropped_Zone length 0 to 381 pixels (x axis)

        Parameters
        ----------
        Rotate (global): string
            this string == "yes" (if images need to be rotated) or "no" (if not)

        target_Folder (global): string
            path of "S.A.R.A Analysis" folder, where all results and images will be saved

        Run_counter : integer
            starts from 0, increments by 1 after each successful analysis of an angle folder (only counts 90 & 270) 

        image_asymmetrie_meniscus : string
            found meniscus in image used for asymmetry calculations
            this string will later be converted to int
            will be later renamed to "Pre_Amp2_meniscus"

        Part_1_checker : boolean
            if combined_results and combined_results_vol was calculated correctly then Part_1_checker = True
            otherwise Part_1_checker = False

        new_image : string
            path of image_asymmetrie image saved in target_Folder
        
        Returns
        -------
        Pre_Amp_Asymmetrie : string
            path of every image to be analyzed in Pre_Amp analysis
            this path now displays the image after transformation

        x (global): integer
            new defined x of the new Cropped_Zone for the right chamber

        y (global): integer
            new defined y of the new Cropped_Zone for the right chamber

        Degrees (global): integer (can be float)
            angle which will be used to rotate the saved image_asymmetrie image, so that meniscus in the right chamber is as horizontal as possible

        Analysis_Folder (global): string
            path of S.A.R.A Analysis folder, identical to target_Folder string, but here renamed so its easier to work with within "Step 2"

        Run_counter : integer
            starts from 0, increments by 1 after each successful analysis of an angle folder (only counts 90 & 270) 

        Pre_Amp1_meniscus : integer
            y coordinate of Pre_Amp1 meniscus in the image_asymmetrie image, but it's local (which is defined by Cropped_Zone)

        real_Pre_Amp1_meniscus : integer
            y coordinate of Pre_Amp1 meniscus in the image_asymmetrie image, but converted to global y, not local (which is defined by Cropped_Zone)
        """

        window_name = "Analyzing_Images"
        Analysis_Folder = target_Folder

        # Redefining Crop_Zone and Rotation values
        x = 1655  # or x + 518
        y = 880  # or y + 380
        Degrees = 344

        Pre_Amp2_meniscus_percentage = (1134 - Pre_Amp2_meniscus) / (
            1134 - 682
        )  # = (A-C)/(A-B) where A is highest point of PreAmp chamber, B lowest point and C is global y of found Meniscus
        Pre_Amp2_volume = preAmpFilltoVolume(
            Pre_Amp2_meniscus_percentage
        )  # converting percentage of filled chamber to volume value

        # Redefining Area_Grayscale to fit new folder, displaying a window showing re-rotated image_95
        def Area_Grayscale_Pre_Amp1(new_image):
            global averages
            global Pre_Amp2_meniscus_PreAmp1_analogue

            """Function finding image containing meniscus at 95 mikroLiter filled position (using the meniscus for left chamber)
               also finds image used for asymmetry calculations (y = 932) 
               this function also saves the images in target_Folder

            Parameters
            ----------
            Analysis_Folder (global): string
                path of S.A.R.A Analysis folder, identical to target_Folder string, but here renamed so its easier to work with within "Step 2"

            new_image : string
                path of image_asymmetrie image saved in target_Folder

            Degrees (global): integer (can be float)
                angle which will be used to rotate the saved image_asymmetrie image, so that meniscus in the right chamber is as horizontal as possible

            Pre_Amp2_meniscus : integer
                found meniscus in image used for asymmetry calculations
                was previously named "image_asymmetrie_meniscus"

            Returns
            -------
            Pre_Amp2_meniscus_PreAmp1_analogue (global): integer
                mirroring PreAmp 2 meniscus on the right chamber, if found Pre_Amp1 meniscus was to be rotated same like PreAmp 1 (Degrees = 344)
                will be used to define a ROI for a more precise location of meniscus for the right chamber

            yKoordinates : list of integers
                list of the restricted ROI of right chamber-defined Cropped_Zone
                this ROI will be used to find the meniscus instead of using the entire Cropped_Zone for the right chamber
                ROI is = +- 20  Pre_Amp2_meniscus_PreAmp1_analogue
                will also be used for plotting

            averages (global): list of floats
                list of all calculated row averages for each row of right chamber-defined Cropped_Zone
                will also be used for plotting
        """
            
            averages = []
            os.chdir(
                Analysis_Folder
            )  # changing working directory to SARA Analysis Ordner
            image_Path = os.path.join(
                Analysis_Folder, new_image
            )  # finding specific paths for all images in SARA Analysis Ordner to be analyzed
            processed_image = cv.imread(image_Path)

            Pre_Amp2_meniscus_PreAmp1_analogue = 90 + int(y2)  # 90 is an arbitrary constanct to bring meniscus to the right position, it works everytime if Pre_Amp2_meniscus was found correctly

            # Redefining cropped zone specifically for PreAmp 1 meniscus detection. It is defined by the just found Pre_Amp2_meniscus_PreAmp1_analogue from it -20 to it  +20
            Cropped_Zone = processed_image[
                Pre_Amp2_meniscus_PreAmp1_analogue
                - 20 : Pre_Amp2_meniscus_PreAmp1_analogue
                + 20,
                x : (x + 14),
            ]

            # Reversing y Coordinates for Pre_Amp1 meniscii with the newly defined cropped zone. THis is done for the plot
            yKoordinates = list(
                reversed(
                    range(
                        Pre_Amp2_meniscus_PreAmp1_analogue - 20,
                        Pre_Amp2_meniscus_PreAmp1_analogue + 20,
                    )
                )
            )

            # For each y calculating x averages(same like PreAmp 2 measurements)
            for Y_Koordinate in Cropped_Zone:
                Rows = [row[1] for row in Y_Koordinate]
                Average_in_row = sum(Rows) / 14
                averages.append(Average_in_row)

            return yKoordinates, averages, Pre_Amp2_meniscus_PreAmp1_analogue

        # Checking if Pre_Amp2 = Part 1 has been analyzed
        if Part_1_checker == True:
            Run_counter += 1

            # Calling functions foor PreAmp 1 Analysis for .jpg images (should only be 1 image)
            for new_image in glob.iglob(os.path.join(Analysis_Folder, "*.jpg")):

                # Running scipy.ndimage.rotate and tracking Pre_Amp2_meniscus after rotation
                new_height, new_width, img_rot, (x2, y2) = Rotate_Pre_Amp1(
                    new_image,
                    np.array([1208, Pre_Amp2_meniscus]),
                    (Degrees),
                )

                (
                    yKoordinates,
                    averages,
                    Pre_Amp2_meniscus_PreAmp1_analogue,
                ) = Area_Grayscale_Pre_Amp1(new_image)

                # Calculating average grayscale value for every row in new cropped zone
                Pre_Amp1_average = Process_Image(averages)  # Derivative of 1-array

                # Turning local y coordinate into global
                Pre_Amp1_meniscus = (
                    Pre_Amp2_meniscus_PreAmp1_analogue - 20
                ) + np.argmax(Pre_Amp1_average)

                Pre_Amp1_meniscus_percentage = (1527 - Pre_Amp1_meniscus) / (
                    1527 - 1077
                )  # = (A-C)/(A-B) where A is highest point of PreAmp chamber, B lowest point and C is global y of found Meniscus
                Pre_Amp1_volume = preAmpFilltoVolume(
                    Pre_Amp1_meniscus_percentage
                )  # converting percentage of filled chamber to volume value

                # Tracking Pre_Amp1_meniscus position pre-rotation and turning it to integer
                new_height, new_width, img_rot, (x2, y1) = Rotate_Pre_Amp1(
                    new_image,
                    np.array([x, Pre_Amp1_meniscus]),
                    (360 - Degrees),
                )

                real_Pre_Amp1_meniscus = Pre_Amp1_meniscus - int(
                    y / 2
                )  # turning local Pre_Amp1 to global Pre_Amp1

                # Drawing the Crop_Zone on the shown window and resizing window so that it shows correctly on screen
                image_Path = os.path.join(Analysis_Folder, new_image)
                processed_image = cv.imread(image_Path)
                cv.rectangle(
                    img=processed_image,
                    pt1=(x, Pre_Amp2_meniscus_PreAmp1_analogue - 20),
                    pt2=(x + 14, Pre_Amp2_meniscus_PreAmp1_analogue + 20),
                    color=(0, 0, 255),
                    thickness=1,
                )
                cv.line(
                    processed_image,
                    (x, Pre_Amp1_meniscus),
                    (x + 14, Pre_Amp1_meniscus),
                    (125, 250, 0),
                    thickness=2,
                )
                cv.namedWindow("Analyzing_Images", cv.WINDOW_NORMAL)
                cv.resizeWindow("Analyzing_Images", 1216, 1936)
                cv.imshow(window_name, processed_image)
                cv.waitKey(
                    15
                )  # it's better to leave this at 0 if you want to stare at the found meniscus for longer, you have to close the window manually this way

                cv.destroyWindow(window_name)

                # Calculating Asymmetry RIGHT/LEFT (PreAmp 1/ PreAmp 2)
                Pre_Amp_Asymmetrie = (
                    real_Pre_Amp1_meniscus / Pre_Amp2_meniscus
                )  # measured by pixel positions
                Pre_Amp_Asymmetrie2 = (
                    Pre_Amp1_volume / Pre_Amp2_volume
                )  # measured by conversions to volume

                # Plotting AverageGrayscale Plot for PreAmp1 only for image_95
                plt.plot(yKoordinates, averages)
                plt.xlabel("Y-Position")
                plt.ylabel("Average Grayscale")
                plt.title("PreAmp_1 AverageGrayscale Plot")
                plt.savefig(
                    Analysis_Folder
                    + "\Pre_Amp1 AverageGrayscale "
                    + str(os.path.basename(Folder_Path))
                    + ".png",
                    dpi=500,
                )
                plt.ion()
                plt.show()
                plt.close()
                ########################################################################################################

                # Creating a .txt file with all the printed info
                def Create_Report_txt():
                    
                    """Function creating a text file and saving it in target_Folder
                       this text file named "Report" contains all the important results after SARA Analysis
                       will be saved separately for each analyzed angle folder (only counting 270 and 90) 
                       will be used to create Report.xlsx file

                    Parameters
                    ----------
                    Folder_Path (global): string
                        path of every strobo run folder
                        this path is identical to Raw_Path, but changes if folder has been renamed 

                    Analysis_Folder (global): string
                        path of S.A.R.A Analysis folder, identical to target_Folder string, but here renamed so its easier to work with within "Step 2"

                    Run_counter : integer
                        starts from 0, increments by 1 after each successful analysis of an angle folder (only counts 90 & 270)

                    Pre_Amp_Asymmetrie : float
                        calculated Asymmetry value using found meniscii, right chamber/left chamber
                        Formula => = real_Pre_Amp1_meniscus / Pre_Amp2_meniscus

                    Pre_Amp_Asymmetrie2 : float
                        calculated Asymmetry value using found volumes, right chamber/left chamber
                        Formula => = Pre_Amp1_volume / Pre_Amp2_volume 

                    Pre_Amp1_volume : float
                        volume of fluid in the right Pre_Amp chamber in image_asymmetrie image

                    Pre_Amp2_volume : float
                        volume of fluid in the left Pre_Amp chamber in image_asymmetrie image
                                                                 
                    Pre_Amp2_meniscus : integer
                        found meniscus in image used for asymmetry calculations
                        was previously named "image_asymmetrie_meniscus"

                    real_Pre_Amp1_meniscus : integer
                        y coordinate of Pre_Amp1 meniscus in the image_asymmetrie image, but converted to global y, not local (which is defined by Cropped_Zone)

                    Inaccessible_Images : list of strings
                        list containing paths of images to not* be analyzed, because they dont have the required dimensions

                    First_Image (global): integer
                        input by user before starting SARA
                        used to find the image with that count within angle folder (only folders 270 and 90)
                        first image to be analyzed for Pre_Amp analysis

                    Last_Image (global): integer
                        input by user before starting SARA
                        used to find the image with that count within angle folder (only folders 270 and 90)  
                        last image to be analyzed for Pre_Amp analysis

                    image_95 (global): string
                        contains the name of image containing meniscus at 95 mikroLiter filled position (with the ".jpg")
                        Example: "Bild 312.jpg"

                    image_max_number : string
                        contains the name of image containing maximum filled Pre_Amp Chamber (without the ".jpg")
                        Example: "Bild 301"

                    max_volume (global): string
                        contains found meniscus for fluid at maximum filled Pre_Amp Chamber
                        this string will later be converted to int

                    combined_results (global): list of integers
                        list containing all the found meniscii position (their y coordinate) during the run

                    zero_seconds : float
                        shows real-world time using function time.time()
                        runs at the time Front_side_analyzer() gets called
                        will be used to calculate how much time SARA need for a full analysis of 1 angle folder (only counts folder 270 and 90)
        
                    Returns
                    -------
                    Report : TextIOWrapper
                        Text file containing all the results from SARA Analysis 
                    """
                            
                    Report_name = (
                        "Report Pre_Amp " + str(os.path.basename(Folder_Path)) + ".txt"
                    )
                    Report_path = os.path.join(Analysis_Folder, Report_name)
                    Report = open(Report_path, "w")
                    # All Run Details
                    Report.write("-" * 70 + "\n")
                    Report.write("*> Pre_Amp Analysis Report <*" + "\n")
                    Report.write("-" * 70 + "\n")
                    Report.write(
                        "-> This is the: #" + str(Run_counter) + " Analysis-Run." + "\n"
                    )
                    Report.write("-" * 70 + "\n")
                    Report.write(
                        "-> Pre_Amp2 Meniscus Position is at: "
                        + str(Pre_Amp2_meniscus)
                        + " (Reference)."
                        + "\n"
                    )
                    Report.write("-" * 70 + "\n")
                    Report.write(
                        "-> Pre_Amp1 Meniscus Position is at: "
                        + str(real_Pre_Amp1_meniscus)
                        + "."
                        + "\n"
                    )
                    Report.write("-" * 70 + "\n")
                    Report.write(
                        "-> There were: "
                        + str(len(Inaccessible_Images))
                        + " inaccessible Images."
                        + "\n"
                    )
                    Report.write("-" * 70 + "\n")
                    Report.write(
                        "-> Your Asymmetry value (pixel) is: "
                        + str(Pre_Amp_Asymmetrie)
                        + "."
                        + "\n"
                    )
                    Report.write("-" * 70 + "\n")
                    Report.write(
                        "-> "
                        + str(Last_Image - First_Image - 1)
                        + " Images were analyzed."
                        + "\n"
                    )
                    Report.write("-" * 70 + "\n")
                    Report.write("-> " + str(image_95) + " contains 95 mikroL." + "\n")
                    Report.write("-" * 70 + "\n")
                    Report.write(
                        "-> " + str(image_max_number) + " contains max volume" + "\n"
                    )
                    Report.write(" Max volume = " + str(max_volume) + " mikroL." + "\n")
                    Report.write("-" * 70 + "\n")
                    Report.write(
                        "-> Your Asymmetry value (volume) is: "
                        + str(Pre_Amp_Asymmetrie2)
                        + "."
                        + "\n"
                    )
                    Report.write("-" * 70 + "\n")
                    Report.write(
                        "-> Volume in Pre_Amp1 is : "
                        + str(Pre_Amp1_volume)
                        + "."
                        + "\n"
                    )
                    Report.write("-" * 70 + "\n")
                    Report.write(
                        "-> Volume in Pre_Amp2 is : "
                        + str(Pre_Amp2_volume)
                        + "."
                        + "\n"
                    )
                    Report.write("-" * 70 + "\n")
                    Report.write(str(combined_results) + "\n")
                    Report.write("-" * 70 + "\n")
                    # Programm IT-Statistics
                    Programm_statistics = psutil.Process(getpid())
                    Report.write(
                        "-> CPU Used: "
                        + str(psutil.cpu_percent(interval=None))
                        + " %."
                        + "\n"
                    )
                    Report.write(
                        "-> RAM Used: "
                        + str(Programm_statistics.memory_percent())
                        + " %."
                        + "\n"
                    )
                    Report.write("-" * 70 + "\n")
                    # Time Statistics
                    global_seconds = time.time()
                    time_elapsed = global_seconds - zero_seconds
                    time_elapsed = "{:.2f}".format(time_elapsed)
                    Report.write(
                        "-> Analysis for this folder was finished in: "
                        + str(time_elapsed)
                        + "s."
                        + "\n"
                    )
                    Report.write(
                        "-> Created on: " + str(time.ctime(global_seconds)) + "." + "\n"
                    )
                    Report.write("-" * 70 + "\n")
                    Report.close()

                    return Report

                Create_Report_txt()

                # Changing previous images in target_Folder to .png so that future function calls can exclude them
                temporary = new_image_asymmetrie.split(".")
                new_image = temporary[0]
                renamed_new_image = new_image + ".png"
                renamed_new_image_path = os.path.join(target_Folder, renamed_new_image)
                os.rename(image_Path, renamed_new_image_path)

                # Printing stuff for python
                print(combined_results)
                print(
                    "*Pre_Amp2 Meniscus Position is at: "
                    + str(Pre_Amp2_meniscus)
                    + " (Reference)"
                )
                print(
                    "*Pre_Amp1 Meniscus Position is at: " + str(real_Pre_Amp1_meniscus)
                )
                print("*Pre_Amp1 Volume is: " + str(Pre_Amp1_volume))
                print("*Pre_Amp2 Volume is: " + str(Pre_Amp2_volume))
                print(
                    "*There were: "
                    + str(len(Inaccessible_Images))
                    + " inaccessible Images."
                )
                print(
                    "*Your Asymmetry value (pixel) is: " + str(Pre_Amp_Asymmetrie) + "."
                )
                print(
                    "*Your Asymmetry value (volume) is: "
                    + str(Pre_Amp_Asymmetrie2)
                    + "."
                )
                print(
                    "* "
                    + str(Last_Image - First_Image - 1)
                    + " Images were analyzed. *"
                    + str(image_95)
                    + " contains 95 mikroL."
                )
                print(
                    " "
                    + str(image_max_number)
                    + " contains max volume = "
                    + str(max_volume)
                    + " mikroL."
                )

                return Pre_Amp_Asymmetrie

    Pre_Amp1_analysis(target_Folder)

    # Stopping total timer, editting run_checker
    finished_seconds = time.time()
    total_time = finished_seconds - absolute_zero_seconds
    Checker = open(Checker_path, "w")
    Checker.write("-" * 70 + "\n")
    Checker.write(
        "-> DELETE THIS FILE IF YOU WANT TO ANALYZE AGAIN OR REMOVE IT FROM THIS FOLDER"
        + "\n"
    )
    Checker.write("\n")
    Checker.write(
        "-> ALSO PLEASE DELETE/REMOVE ALL FILES FROM S.A.R.A Analysis FOLDER" + "\n"
    )
    Checker.write("-" * 70 + "\n")
    Checker.write("-> TOTAL RUNTIME: " + str(total_time) + "s." + "\n")
    Checker.close()
    print("TOTAL RUNTIME: " + str(total_time) + "s.")

    return


Folder_analyzer()

# Creating a .xlsx file with all the results
def Create_Report_xlsx():

    """Function creating a .xlsx file, neatly packing all found and saved results after SARA Analysis which were previously saved in Report.text
       rows represent each angle-folder analyzed (for ex. 270, 90) for each strobo run 
       columns represent different result variables
       fails to run, if Report.txt doesn't exist in correspoding angle folder (only counting 270 & 90)

        Parameters
        ----------
        Main_Path (global): string
            path of MAIN folder (the path which was input when SARA was started)
            MAIN folder contains all strobo run folders to be analyzed

        Returns
        -------
        no specific returns
        saves the editted Report.xlsx file
        """
         
    # Defining all the lists, which will contain all different results
    Run_count_list = []
    Pre_Amp2_meniscus_list = []
    Pre_Amp1_meniscus_list = []
    Pre_Amp_Asymmetrie_pixel_list = []
    Pre_Amp1_volume_list = []
    Pre_Amp2_volume_list = []
    Pre_Amp_Asymmetrie_volume_list = []
    Inaccessible_Images_list = []
    Count_Images_analyzed_list = []
    Image_95_number_list = []
    Image_max_number_list = []
    Max_volume_list = []

    # Finding the .txt files within folders within main_path (cascade)
    for root, dirs, folders in os.walk(Main_Path):
        for parent in dirs:
            Parent_path = os.path.join(Main_Path, parent)
            folder_list = os.listdir(Parent_path)

            for folder in folder_list:
                folder_path = os.path.join(Parent_path, folder)

                if folder == "S.A.R.A Analysis":
                    os.chdir(folder_path)
                    file_list = os.listdir(folder_path)
                    for files in file_list:
                        if files.endswith(".txt"):
                            Report = open(files, "r")

                            # Reading every line in .txt file and find only numbers from specified line[i]. These lines contain the results
                            for i, line in enumerate(Report):
                                if i == 3:
                                    Run_count_i = re.findall(r"\d+", line)
                                    Run_count_list.append(int(Run_count_i[0]))

                                if i == 5:
                                    Pre_Amp2_meniscus_i = re.findall(r"\d+", line)
                                    Pre_Amp2_meniscus_list.append(
                                        int(Pre_Amp2_meniscus_i[1])
                                    )

                                if i == 7:
                                    Pre_Amp1_meniscus_i = re.findall(r"\d+", line)
                                    Pre_Amp1_meniscus_list.append(
                                        int(Pre_Amp1_meniscus_i[1])
                                    )

                                if i == 9:
                                    Inaccessible_Images_i = re.findall(r"\d+", line)
                                    Inaccessible_Images_list.append(
                                        int(Inaccessible_Images_i[0])
                                    )

                                if i == 11:
                                    Pre_Amp_Asymmetrie_pixel_i = re.findall(
                                        r"[-+]?(?:\d*\.*\d+)", line
                                    )  # only float
                                    Pre_Amp_Asymmetrie_pixel_list.append(
                                        float(Pre_Amp_Asymmetrie_pixel_i[0])
                                    )

                                if i == 13:
                                    Count_Images_analyzed_i = re.findall(r"\d+", line)
                                    Count_Images_analyzed_list.append(
                                        int(Count_Images_analyzed_i[0])
                                    )

                                if i == 15:
                                    Image_95_number_i = re.findall(r"\d+", line)
                                    Image_95_number_list.append(
                                        int(Image_95_number_i[0])
                                    )

                                if i == 17:
                                    Image_max_number_i = re.findall(r"\d+", line)
                                    Image_max_number_list.append(
                                        int(Image_max_number_i[0])
                                    )

                                if i == 18:
                                    Max_volume_i = re.findall(
                                        r"[-+]?(?:\d*\.*\d+)", line
                                    )  # only float
                                    Max_volume_list.append(float(Max_volume_i[0]))

                                if i == 20:
                                    Pre_Amp_Asymmetrie_volume_i = re.findall(
                                        r"[-+]?(?:\d*\.*\d+)", line
                                    )  # only float
                                    Pre_Amp_Asymmetrie_volume_list.append(
                                        float(Pre_Amp_Asymmetrie_volume_i[0])
                                    )

                                if i == 22:
                                    Pre_Amp1_volume_i = re.findall(
                                        r"[-+]?(?:\d*\.*\d+)", line
                                    )  # only float
                                    Pre_Amp1_volume_list.append(
                                        float(Pre_Amp1_volume_i[1])
                                    )

                                if i == 24:
                                    Pre_Amp2_volume_i = re.findall(
                                        r"[-+]?(?:\d*\.*\d+)", line
                                    )  # only float
                                    Pre_Amp2_volume_list.append(
                                        float(Pre_Amp2_volume_i[1])
                                    )

                            Report.close()

        # xlsxwriter doesn't need a specific path, it works within current directory. So here we change chdir
        os.chdir(Main_Path)
        workbook_name = "SARA Report.xlsx"
        workbook = xlsxwriter.Workbook(workbook_name)
        worksheet = workbook.add_worksheet(
            "First_Sheet"
        )  # creating the first sheet within the excel workbook

        Angles_list = [
            270,
            90,
        ] * 400  # Creating a list of 270 then 90 (repeating 400 times to include as many runs as possible) because python always analyzes 270 degrees before 90 degrees

        # Defining all the columns within the excel sheet
        worksheet.write(0, 0, "#")
        worksheet.write(0, 1, "Angle")
        worksheet.write(0, 2, "Run_count")
        worksheet.write(0, 3, "Pre_Amp2_meniscus")
        worksheet.write(0, 4, "Pre_Amp1_meniscus")
        worksheet.write(0, 5, "Pre_Amp_Asymmetrie(pixel)")
        worksheet.write(0, 6, "Pre_Amp1_volume")
        worksheet.write(0, 7, "Pre_Amp2_volume")
        worksheet.write(0, 8, "Pre_Amp_Asymmetrie(volume)")
        worksheet.write(0, 9, "Inaccessible_Images")
        worksheet.write(0, 10, "Count_Images_analyzed")
        worksheet.write(0, 11, "Image_95_number")
        worksheet.write(0, 12, "Image_max_number")
        worksheet.write(0, 13, "Max_volume")

        # Zipping all data, so that they can be imported by excel
        values = (
            Angles_list,
            Run_count_list,
            Pre_Amp2_meniscus_list,
            Pre_Amp1_meniscus_list,
            Pre_Amp_Asymmetrie_pixel_list,
            Pre_Amp1_volume_list,
            Pre_Amp2_volume_list,
            Pre_Amp_Asymmetrie_volume_list,
            Inaccessible_Images_list,
            Count_Images_analyzed_list,
            Image_95_number_list,
            Image_max_number_list,
            Max_volume_list,
        )

        variables = [
            "Angle",
            "Run_count",
            "Pre_Amp2_meniscus",
            "Pre_Amp1_meniscus",
            "Pre_Amp_Asymmetrie(pixel)",
            "Pre_Amp1_volume",
            "Pre_Amp2_volume",
            "Pre_Amp_Asymmetrie(volume)",
            "Inaccessible_Images",
            "Count_Images_analyzed",
            "Image_95_number",
            "Image_max_number",
            "Max_volume",
        ]
        data = dict(zip(variables, values))
        data_conv = [
            {key: value[i] for key, value in data.items()}
            for i in range(len(Run_count_list))
        ]  # converting dictonary of lists to list of dictonaries

        # Importing all results from defined lists into defined columns
        for index, entry in enumerate(data_conv):
            worksheet.write(index + 1, 0, str(index))
            worksheet.write(index + 1, 1, entry["Angle"])
            worksheet.write(index + 1, 2, entry["Run_count"])
            worksheet.write(index + 1, 3, entry["Pre_Amp2_meniscus"])
            worksheet.write(index + 1, 4, entry["Pre_Amp1_meniscus"])
            worksheet.write(index + 1, 5, entry["Pre_Amp_Asymmetrie(pixel)"])
            worksheet.write(index + 1, 6, entry["Pre_Amp1_volume"])
            worksheet.write(index + 1, 7, entry["Pre_Amp2_volume"])
            worksheet.write(index + 1, 8, entry["Pre_Amp_Asymmetrie(volume)"])
            worksheet.write(index + 1, 9, entry["Inaccessible_Images"])
            worksheet.write(index + 1, 10, entry["Count_Images_analyzed"])
            worksheet.write(index + 1, 11, entry["Image_95_number"])
            worksheet.write(index + 1, 12, entry["Image_max_number"])
            worksheet.write(index + 1, 13, entry["Max_volume"])

        workbook.close()  # Closes opened excel file

        return


Create_Report_xlsx()

#############AUTHOR:############semibrami@gmail.com#####################################